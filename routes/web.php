<?php

use App\Http\Controllers\Dashboard\EncashmentController;
use App\Http\Controllers\Dashboard\GenealogyController;
use App\Http\Controllers\Dashboard\HomeController as DashboardHomeController;
use App\Http\Controllers\Dashboard\Settings\ChangePasswordController;
use App\Http\Controllers\Dashboard\Settings\ProfileController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::middleware(['auth'])
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::get('', [DashboardHomeController::class, 'index'])->name('home');
        Route::get('genealogy', [GenealogyController::class, 'index'])->name('genealogy');
        Route::get('encashment', [EncashmentController::class, 'index'])->name('encashment');
        Route::post('encashment', [EncashmentController::class, 'store']);

        Route::prefix('settings')
            ->name('settings.')
            ->group(function () {
                Route::get('change-password', [ChangePasswordController::class, 'index'])->name('change-password');
                Route::put('change-password', [ChangePasswordController::class, 'update']);

                Route::get('profile', [ProfileController::class, 'index'])->name('profile');
                Route::put('profile', [ProfileController::class, 'update']);
                Route::post('profile/image', [ProfileController::class, 'uploadImage'])->name('profile.image');
            });
    });

require __DIR__ . '/auth.php';
require __DIR__ . '/admin.php';
