<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\IncomesController;
use App\Http\Controllers\Admin\PaymentsController;
use App\Http\Controllers\Admin\PrimesController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\WithdrawalsController;
use App\Http\Controllers\LivewireFrame\LivewireFrameController;
use Illuminate\Support\Facades\Route;


Route::prefix('vemma/admin')
    ->name('admin.')
    ->group(function () {

        Route::middleware('guest-admin')
            ->group(function () {
                Route::get('/login', [LoginController::class, 'create'])->name('login');

                Route::post('/login', [LoginController::class, 'store']);
            });

        Route::post('/logout', [LoginController::class, 'destroy'])
            ->middleware('auth-admin:admin')
            ->name('logout');

        Route::middleware('auth-admin:admin')
            ->group(function () {
                Route::get('', [HomeController::class, 'index'])->name('home');
                Route::get('users', [UsersController::class, 'index'])->name('users');
                Route::get('users/{username}', [UsersController::class, 'show'])->name('users.show');
                Route::put('users/{username}/method', [UsersController::class, 'storeMethod'])->name('users.show.method');
                Route::delete('users/method/{id}/', [UsersController::class, 'deleteMethod'])->name('users.show.method.delete');

                Route::get('users/bonus/{user}', [UsersController::class, 'userBonus'])->name('users.bonus');
                Route::post('users/bonus/{user}/{prime}', [UsersController::class, 'storeUserBonus']);

                Route::get('payments', [PaymentsController::class, 'index'])->name('payments');
                Route::post('payments/{user}', [PaymentsController::class, 'store'])->name('payments.store');
                Route::put('payments/options', [PaymentsController::class, 'storeOptions'])->name('payments.options');

                Route::put('payments/method', [PaymentsController::class, 'storeMethod'])->name('payments.method');
                Route::delete('payments/method/{id}', [PaymentsController::class, 'deleteMethod'])->name('payments.method.delete');

                Route::get('withdrawals', [WithdrawalsController::class, 'index'])->name('withdrawals');

                Route::get('incomes', [IncomesController::class, 'index'])->name('incomes');

                Route::get('primes', [PrimesController::class, 'index'])->name('primes');
                Route::post('primes', [PrimesController::class, 'storePrime']);
                Route::delete('primes/{id}', [PrimesController::class, 'destroyPrime'])->name('primes.delete');

                Route::post('advantages', [PrimesController::class, 'storeAdvantage'])->name('advantages');
                Route::delete('advantages/{id}', [PrimesController::class, 'destroyAdvantage'])->name('advantages.delete');

                Route::get('profile', [ProfileController::class, 'index'])->name('profile');
                Route::put('profile', [ProfileController::class, 'update']);
                Route::put('profile/password', [ProfileController::class, 'changePassword'])->name('profile.password');

                Route::get('settings', [SettingsController::class, 'index'])->name('settings');
                Route::post('settings/admins', [SettingsController::class, 'store'])->name('settings.admins');
                Route::post('settings/admins/{admin}', [SettingsController::class, 'updatePassword'])->name('settings.admins.password');
            });
    });


Route::middleware('auth-admin:admin')
    ->prefix('livewire-frame')
    ->group(function () {
        Route::get('', LivewireFrameController::class)->name('livewire-frame');
    });
