# Wemma Web

## Outils

Le principal outil de stucturation de ce projet est Laravel.

### - Laravel
Laravel est framework web fait par le language de programmation PHP, pour plus d'information veuillez visiter le site officiel: www.laravel.com.


### - Autres
Comme dans autres grands projects, celui-ci a utiliser plusieurs composants, library etc. de sorte de vous sentir a l'aise dans la contibution de ce project, veuillez regarder dans les deux principaux fichiers de gestionnaire de packages:
- package.json, pour les libraries nodejs avec NPM | Yarn
- composer.json, pour les liraries php avec Composer

## Installation (Démarrage rapide)
- `git clone https://pngwasi@bitbucket.org/pngwasi/vemma.git`
- `cd vemma`
- `cp .env.example .env`
- `composer install`
- `php artisan key:generate`
- Edit .env
- creer une base de donnee avec mysql (requis)
- `php artisan migrate`
- `npm install`
- `npm run prod`
- Demarer le server web `php artisan serve`

## Assets Bundler
Pour la compilation des assets:

- Development (Vitejs)
- Production (WebPack).

### Compilation

- `npn run prod` pour la production

### development

- `npm run dev` a enter une seule foi
- Server web `http://locahost:8000`

## Back Office

Dans la partie administration, y a pas grand chose a dire mis a part le route d'entre est sur le path: `/dash/login`

## License
MIT
