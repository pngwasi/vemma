<?php

namespace Tests\Feature\Livewire;

use App\Models\Admin;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LivewireFrameTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_livewire_frame_request_without_component_query()
    {
        $auth = Admin::factory()->create();

        $response = $this->actingAs($auth, 'admin')->get('/livewire-frame');

        $response->assertStatus(400);
    }
}
