<?php

namespace Tests\Feature\Dashboard;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_dash_home_screen_rendered()
    {
        $auth = User::factory()->hasProfile()->create();

        $response = $this->actingAs($auth)->get('/dashboard');

        $response->assertStatus(200);
    }
}
