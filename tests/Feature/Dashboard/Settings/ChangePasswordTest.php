<?php

namespace Tests\Feature\Dashboard\Settings;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ChangePasswordTest extends TestCase
{
    use RefreshDatabase;

    public function test_change_password_screen_rendered()
    {
        $user = User::factory()->hasProfile()->create();

        $response = $this->actingAs($user)->get('dashboard/settings/change-password');

        $response->assertStatus(200);
    }

    public function test_validate_new_password_and_save()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->put('dashboard/settings/change-password', [
            'password' => 'password',
            'new_password' => 'newpassword',
            'new_password_confirmation' => 'newpassword'
        ]);

        $this->assertTrue(Hash::check('newpassword', $user->fresh()->password));

        $response->assertSessionHas('updated');

        $response->assertSessionHasNoErrors();

        $response->assertStatus(302);
    }

    public function test_invalide_user_password()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->put('dashboard/settings/change-password', [
            'password' => 'wrong-password',
            'new_password' => 'newpassword',
            'new_password' => 'newpassword'
        ]);

        $response->assertSessionHasErrors(['password']);

        $response->assertStatus(302);
    }
}
