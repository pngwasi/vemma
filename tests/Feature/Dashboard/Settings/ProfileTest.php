<?php

namespace Tests\Feature\Dashboard\Settings;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_settings_profile_screen_rendered()
    {
        $user = User::factory()->hasProfile()->create();

        $response = $this->actingAs($user)->get('dashboard/settings/profile');

        $response->assertStatus(200);
    }


    public function test_upload_user_image_profile()
    {
        Storage::fake('public');

        $user = User::factory()->hasProfile()->create();

        $file = UploadedFile::fake()->image('avatar.jpg', 1080, 800)->size(500);

        $response = $this->actingAs($user)->post('dashboard/settings/profile/image', [
            'image' => $file,
        ]);

        $response->assertSessionHasNoErrors();

        /** @var mixed */
        $disk = Storage::disk('public');

        $filename = "{$user->username}.jpg";

        $disk->assertExists("images/{$filename}");

        $this->assertStringContainsString($filename, $user->fresh()->profile->image);

        $response->assertStatus(302);
    }

    public function test_settings_user_profile_put_modifications()
    {
        $user = User::factory()->hasProfile()->create();
        $oldFn = $user->profile->firstname;

        $response = $this->actingAs($user)->put('dashboard/settings/profile', [
            'firstname' => $this->faker->unique()->firstName,
            'lastname' => $this->faker->lastName,
            'middlename' => $this->faker->lastName . "-middle",
            'gender' => 'male',
            'phone' => $this->faker->unique()->phoneNumber,
            'birthdate' => $this->faker->date(),
            'tax_identification_number' => null,
            'address' => $this->faker->address,
            'country' => $this->faker->country,
            'region' => $this->faker->streetAddress,
            'description' => $this->faker->text
        ]);

        $response->assertSessionHasNoErrors();

        $this->assertTrue($oldFn !== $user->profile->fresh()->firstname);

        $response->assertSessionHas('updated');

        $response->assertStatus(302);
    }
}
