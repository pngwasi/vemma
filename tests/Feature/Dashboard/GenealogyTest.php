<?php

namespace Tests\Feature\Dashboard;

use App\Models\Genealogy;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class GenealogyTest extends TestCase
{
    use RefreshDatabase;


    public function test_genealogy_screen_rendered()
    {
        $user = User::factory()->hasProfile()->create();

        $response = $this->actingAs($user)->get('/dashboard/genealogy');

        $response->assertViewHas(['genealogies', 'genealogiesChart']);

        $genealogies = $response->viewData('genealogies');

        $response->assertStatus(200);

        return $genealogies;
    }

    /**
     * @depends test_genealogy_screen_rendered
     */
    public function test_genealogy_data_view_returned(array $genealogies)
    {
        $this->assertIsArray($genealogies);

        $this->assertCount(13, $genealogies);

        $this->assertContainsEquals(null, $genealogies);
    }


    public function test_genealogy_tree_data_has_been_respected()
    {
        $downline1 = User::factory()->hasProfile()->create();

        $downline2 = User::factory()
            ->has(
                Genealogy::factory()
                    ->state(function (array $attributes, User $user) use ($downline1) {
                        return ['upline_id' => $user->id, 'user_id' => $downline1->id];
                    }),
                'downlines'
            )
            ->hasProfile()
            ->create();

        $uroot = User::factory()
            ->has(
                Genealogy::factory()
                    ->state(function (array $attributes, User $user) use ($downline2) {
                        return ['upline_id' => $user->id, 'user_id' => $downline2->id];
                    }),
                'downlines'
            )
            ->hasProfile()
            ->create();

        $response = $this->actingAs($uroot)->get('/dashboard/genealogy');

        $genealogies = collect($response->viewData('genealogies'))
            ->map(fn ($g) => $g ? $g->id : $g)
            ->all();

        $this->assertTrue(array_search($uroot->id, $genealogies) == 0);

        $this->assertTrue(array_search($downline2->id, $genealogies) == 1);

        $this->assertTrue(array_search($downline1->id, $genealogies) == 4);
    }
}
