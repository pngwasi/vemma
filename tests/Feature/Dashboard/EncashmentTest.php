<?php

namespace Tests\Feature\Dashboard;

use App\Models\Balance;
use App\Models\EncashmentMethod;
use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Tests\TestCase;

class EncashmentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_encashment_screen_rendered()
    {
        $auth = User::factory()->hasProfile()->create();

        $response = $this->actingAs($auth)->get('/dashboard/encashment');

        $response->assertViewHas(['balance', 'methods', 'encashments']);

        $response->assertStatus(200);
    }


    public function actingAuthUser(array $state = [])
    {
        PaymentMethod::factory()
            ->count(2)
            ->state(new Sequence(
                ['method' => 'cheque'],
                ['method' => 'bank'],
            ))->create();

        $user = User::factory()
            ->state($state)
            ->hasEncashments()
            ->hasBalances()
            ->has(
                EncashmentMethod::factory()
                    ->state(function () {
                        return [
                            'method' => 'cheque',
                            'account_number' => 'cheque',
                            'currency' => 'dollar'
                        ];
                    })
            )->create();

        return $user;
    }


    public function test_user_encashment_process_with_inactive_aamout()
    {
        $user = $this->actingAuthUser();

        $response = $this->actingAs($user)->post('/dashboard/encashment', [
            'method' => 'cheque',
            'amount' => 100
        ]);

        $response->assertSessionHasErrors(['amount' => "Vous devez activer votre compte avant de continuer."]);

        $response->assertStatus(302);
    }

    public function test_user_encashment_process_with_no_balance()
    {
        $user = $this->actingAuthUser(['active' => true]);

        $response = $this->actingAs($user)->post('/dashboard/encashment', [
            'method' => 'cheque',
            'amount' => 100
        ]);

        $response->assertSessionHasErrors(['amount']);

        $response->assertStatus(302);
    }

    public function test_user_encashment_process()
    {
        $user = $this->actingAuthUser(['active' => true]);

        Balance::factory()->create([
            'amount' => 200,
            'user_id' => $user->id,
            'confirmed' => true
        ]);

        $response = $this->actingAs($user)->post('/dashboard/encashment', [
            'method' => 'cheque',
            'amount' => 100
        ]);

        $response->assertSessionHasNoErrors();

        $response->assertSessionHas('proceed');

        $response->assertStatus(302);

        return $user->fresh()->encashments;
    }

    /**
     * @depends test_user_encashment_process
     */
    public function test_user_encashment_stored(Collection $encashments)
    {
        $this->assertNotEmpty($encashments);
    }
}
