<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    public function test_registration_screen_can_be_rendered()
    {
        $user = User::factory()->hasProfile()->create();

        $response = $this->get("/register?upline={$user->username}");

        $response->assertStatus(200);
    }


    public function test_has_not_upline_query_on_registation_url()
    {
        $response = $this->get("/register");

        $response->assertForbidden();
    }

    public function test_new_users_can_register()
    {
        Event::fake();

        $user = User::factory()->create();

        $response = $this->post("/register?upline={$user->username}", [
            'firstname' => 'joe',
            'lastname' => 'doe',
            'email' => 'joe@gmail.com',
            'password' => 'password',
            'accepted' => true,
            'password_confirmation' => 'password',
        ]);

        $this->assertAuthenticated();

        Event::assertDispatched(Registered::class);

        $response->assertRedirect(route('verification.notice', [], false));
    }
}
