<?php

namespace Tests\Feature\Admin;

use App\Models\Admin;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ProfileEditTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_profile_screen_can_be_rendered()
    {
        $auth = Admin::factory()->create();

        $response = $this->actingAs($auth, 'admin')->get('/vemma/admin/profile');

        $response->assertStatus(200);
    }


    public function test_user_profile_edit_name_and_exlude_email_edition()
    {
        $auth = Admin::factory()->create();

        $response = $this->actingAs($auth, 'admin')->put('/vemma/admin/profile', [
            'name' => 'demo name',
            'email' => 'demo@gmail.com'
        ]);

        $response->assertSessionHasNoErrors();

        $this->assertTrue($auth->fresh()->email !== 'demo@gmail.com');

        $response->assertStatus(302);

        $response->assertSessionHas('updated');
    }


    public function test_validate_new_password_and_save()
    {
        $user = Admin::factory()->create();

        $response = $this->actingAs($user, 'admin')->put('/vemma/admin/profile/password', [
            'password' => 'password',
            'new_password' => 'newpassword',
            'new_password_confirmation' => 'newpassword'
        ]);

        $this->assertTrue(Hash::check('newpassword', $user->fresh()->password));

        $response->assertSessionHas('updated');

        $response->assertSessionHasNoErrors();

        $response->assertStatus(302);
    }

    public function test_invalide_user_password()
    {
        $user = Admin::factory()->create();

        $response = $this->actingAs($user, 'admin')->put('/vemma/admin/profile/password', [
            'password' => 'wrong-password',
            'new_password' => 'newpassword',
            'new_password' => 'newpassword'
        ]);

        $response->assertSessionHasErrors(['password']);

        $response->assertStatus(302);
    }
}
