<?php

namespace Tests\Feature\Admin;

use App\Models\Admin;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterNewAdminTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_registration_screen_can_be_rendered()
    {
        $auth = Admin::factory()->create();

        $response = $this->actingAs($auth, 'admin')->get('/vemma/admin/settings');

        $response->assertStatus(200);
    }

    public function test_new_users_can_register_by_simple_admin()
    {
        $user = Admin::factory()->create();

        $response = $this->actingAs($user, 'admin')->post("/vemma/admin/settings/admins", [
            'name' => 'joe doe',
            'email' => 'joe@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $response->assertSessionHas('error');
    }


    public function test_new_users_can_register()
    {
        $user = Admin::factory()->state(['super' => true])->create();

        $response = $this->actingAs($user, 'admin')->post("/vemma/admin/settings/admins", [
            'name' => 'joe doe',
            'email' => 'joe@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $response->assertSessionHas('created');

        $response->assertCreated();
    }
}
