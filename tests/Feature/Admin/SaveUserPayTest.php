<?php

namespace Tests\Feature\Admin;

use App\Models\Admin;
use App\Models\Genealogy;
use App\Models\Income;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\User;
use App\Providers\AppServiceProvider;
use App\Repositories\SettingsRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SaveUserPayTest extends TestCase
{
    use RefreshDatabase;


    public function test_payments_screen_can_be_rendered()
    {
        $auth = Admin::factory()->create();

        $response = $this->actingAs($auth, 'admin')->get('/vemma/admin/payments');

        $response->assertStatus(200);
    }

    public function test_save_user_pay_without_payment_settings()
    {
        $auth = Admin::factory()->state(['super' => true])->create();

        $user = User::factory()->create();

        $response = $this->actingAs($auth, 'admin')->postJson("/vemma/admin/payments/{$user->id}");

        $response->assertJsonPath('errors.amount', "Aucun paramètre n'a été défini pour les enregistrements de paie.");

        $response->assertStatus(422);
    }


    private function setting()
    {
        Setting::factory()->state(['user_payment' => 30, 'user_percentage' => 20])->create();
    }

    public function test_save_user_pay_by_no_super_admin()
    {
        $this->setting();

        $auth = Admin::factory()->create();

        $user = User::factory()->create();

        $response = $this->actingAs($auth, 'admin')->postJson("/vemma/admin/payments/{$user->id}");

        $response->assertStatus(422);
    }


    public function test_cannot_save_user_if_already_saved()
    {
        $this->setting();

        $auth = Admin::factory()->state(['super' => true])->create();

        $user = User::factory()
            ->has(
                Payment::factory()
                    ->count(1)
                    ->state(function (array $attributes, User $user) {
                        return ['user_id' => $user->id, 'confirmed' => true];
                    }),
                'payment'
            )
            ->create();

        $response = $this->actingAs($auth, 'admin')->postJson("/vemma/admin/payments/{$user->id}");

        $response->assertJsonPath('errors.amount', "Le paiement de l'utilisateur a déjà été enregistré.");

        $response->assertStatus(422);
    }

    public function test_save_user_pay_with_user_event_pay()
    {
        $this->setting();

        $upline = User::factory()->create();

        $auth = Admin::factory()->state(['super' => true])->create();

        $childTree = AppServiceProvider::$CHILD_TREE;

        for ($i = 1; $i <= $childTree; $i++) {

            $user = User::factory()->create();

            Genealogy::factory()->state(['upline_id' => $upline->id, 'user_id' => $user->id])->create();

            $response = $this->actingAs($auth, 'admin')->postJson("/vemma/admin/payments/{$user->id}");

            $this->assertTrue(boolval($user->fresh()->active));

            $response->assertStatus(201);
        }

        $setting = SettingsRepository::getSetting();

        $this->assertNotEmpty($upline->balances);

        $total = $upline->user_downlines->sum(function ($user) {
            return $user->payment->amount;
        });

        $treeAmount = $setting->user_percentage * $total  / 100;

        $this->assertTrue($upline->balances[0]->amount == $treeAmount);

        //test incomes amount balance was saved
        $income = Income::firstWhere('user_id', $upline->id);

        $this->assertNotNull($income);
        $this->assertCount($childTree, Income::all());

        $reste = ($total - $treeAmount) / $childTree;

        $this->assertTrue($income->amount == $reste);
        $this->assertTrue($income->winning_percentage == ($reste * 100 / ($total / $childTree)));
    }
}
