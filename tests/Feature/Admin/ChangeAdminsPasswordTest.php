<?php

namespace Tests\Feature\Admin;

use App\Models\Admin;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ChangeAdminsPasswordTest extends TestCase
{

    use RefreshDatabase;

    public function test_change_admin_password()
    {
        $auth = Admin::factory()->state(['super' => true])->create();

        $user = Admin::factory()->create();

        $response = $this->actingAs($auth, 'admin')->postJson("/vemma/admin/settings/admins/{$user->id}", [
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword'
        ]);

        $this->assertTrue(Hash::check('newpassword', $user->fresh()->password));

        $response->assertSessionHasNoErrors();

        $response->assertStatus(200);
    }

    public function test_cannot_change_password_the_actual_authenticated_user()
    {
        $user = Admin::factory()->state(['super' => true])->create();

        $response = $this->actingAs($user, 'admin')->postJson("/vemma/admin/settings/admins/{$user->id}", [
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword'
        ]);

        $response->assertJsonPath('errors.password', "Vous ne pouvez pas modifier le mot de passe de l'utilisateur actuellement connecté.");

        $response->assertStatus(422);
    }


    public function test_change_any_admin_password_by_no_super_admin()
    {
        $auth = Admin::factory()->create();

        $user = Admin::factory()->create();

        $response = $this->actingAs($auth, 'admin')->postJson("/vemma/admin/settings/admins/{$user->id}", [
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword'
        ]);

        $response->assertStatus(422);
    }
}
