<?php

namespace Tests\Feature\Admin;

use App\Models\Admin;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticatedHomePageTest extends TestCase
{
    use RefreshDatabase;

    public function test_redirect_when_not_authenticated()
    {
        $response = $this->get('/vemma/admin');

        $response->assertStatus(302);

        $this->assertGuest('admin');

        $response->assertRedirect('/vemma/admin/login');
    }
}
