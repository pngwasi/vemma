<?php

namespace App\Notifications;

use App\Models\Prime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserPrimeNotification extends Notification
{
    use Queueable;

    private $prime;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Prime $prime)
    {
        $this->prime = $prime;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'downlines' => $this->prime->downlines,
            'label' => $this->prime->label,
            'message' => "Félicitation, grâce à vos {$this->prime->downlines} downlines, vous avez débloquer votre {$this->prime->label} en bonus."
        ];
    }
}
