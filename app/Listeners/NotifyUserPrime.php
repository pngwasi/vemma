<?php

namespace App\Listeners;

use App\Events\UserPrimeEvent;
use App\Notifications\UserPrimeNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyUserPrime
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserPrimeEvent $event)
    {
        $event->getUser()
            ->notify(new UserPrimeNotification($event->getPrime()));
    }
}
