<?php

namespace App\Listeners\Payment;

use App\Events\Payment\UserPayEvent;
use App\Models\Income;
use App\Models\Payment;
use App\Models\PendingBalance;
use App\Models\Setting;
use App\Models\User;
use App\Providers\AppServiceProvider;
use App\Repositories\SettingsRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;

class IncreaseUplineTreeAndIncomesAmount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserPayEvent $event)
    {
        $upline = $event->getUpline();
        $setting = SettingsRepository::getSetting();

        $downlines = $upline->downlinesPad();

        $isCompletedTreePayment = $upline->hasCompletedTree();

        if ($isCompletedTreePayment) {
            $this->saveUserBalance($upline, $downlines, $setting);
        }

        $this->saveIncomesAmount($upline, $setting, $event->getPayment());
    }

    private function saveIncomesAmount(User $upline, Setting $setting, Payment $payment)
    {
        $winning_percentage = (100 - $setting->user_percentage);

        Income::create([
            'user_id' => $upline->id,
            'amount' => $winning_percentage * $payment->amount / 100,
            'winning_percentage' => $winning_percentage,
            'percentage' => $setting->user_percentage,
            'cross' => $payment->amount
        ]);
    }

    private function saveUserBalance(User $upline, Collection $downlines, Setting $setting)
    {
        $total = $downlines->sum(fn (User $u) => $u->payment->amount);

        $upline->balances()->create([
            'confirmed' => true,
            'amount' => $setting->user_percentage * $total / 100,
            'percentage' => $setting->user_percentage,
            'cross' => $total
        ]);

        $this->updateFromPendingBalance($upline);
    }

    public function updateFromPendingBalance(User $upline)
    {
        $upline->pendingBalances()
            ->get()
            ->each(function (PendingBalance $pending) use ($upline) {
                $upline->balances()->create([
                    'confirmed' => true,
                    'amount' => $pending->amount,
                    'percentage' => $pending->percentage,
                    'cross' => $pending->cross
                ]);
                $pending->delete();
            });
    }
}
