<?php

namespace App\Listeners\Payment;

use App\Events\UserAdvantageEvent;
use App\Repositories\SettingsRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddUserAdvantageBalance
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserAdvantageEvent $event)
    {
        $user = $event->getUser();
        $advantage = $event->getAdvantage();

        $setting = SettingsRepository::getSetting();

        $data = [
            'confirmed' => true,
            'amount' => $advantage->amount,
            'cross' => $advantage->amount,
            'percentage' => $setting->user_percentage,
        ];

        $hasCompletedTree = $user->hasCompletedTree();

        if ($hasCompletedTree) {
            $user->balances()->create($data);
        } else {
            $user->pendingBalances()->create($data);
        }
    }
}
