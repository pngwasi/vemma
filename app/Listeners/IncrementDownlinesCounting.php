<?php

namespace App\Listeners;

use App\Events\Payment\UserPayEvent;
use App\Events\UserAdvantageEvent;
use App\Events\UserPrimeEvent;
use App\Models\Advantage;
use App\Models\Prime;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IncrementDownlinesCounting implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var bool
     */
    public $afterCommit = true;

    /**
     * The number of times the queued listener may be attempted.
     *
     * @var int
     */
    public $tries = 0;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    private function metadata(): array
    {
        $primes = [];
        $advantages = [];

        Prime::all()
            ->each(function (Prime $p) use (&$primes) {
                $primes["{$p->downlines}"] = $p->id;
            });

        Advantage::all()
            ->each(function (Advantage $p) use (&$advantages) {
                $advantages["{$p->downlines}"] = $p->id;
            });

        return [$primes, $advantages];
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserPayEvent $event)
    {
        $user = $event->getUpline();

        [$primes, $advantages] = $this->metadata();

        while ($user) {
            $user = $this->incrementDownlines($user, $primes, $advantages);
        }
    }

    private function incrementDownlines(User $user, array $primes, array $advantages): ?User
    {
        $dcount = $user->downlinesCount ?: $user->downlinesCount()->create();
        $count = $dcount->count + 1;

        $dcount->count = $count;
        $dcount->save();

        $this->dispach($user, strval($count), $primes, $advantages);

        return $user->user_upline;
    }


    private function dispach(User $user, string $count, array $primes, array $advantages)
    {
        if (isset($primes[$count])) {
            event(new UserPrimeEvent(
                $user,
                Prime::find($primes["{$count}"])
            ));
        }

        if (isset($advantages[$count])) {
            event(new UserAdvantageEvent(
                $user,
                Advantage::find($advantages["{$count}"])
            ));
        }
    }
}
