<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\AppServiceProvider;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $upline = $this->hasUpline($request->query('upline'));

        return view('pages.auth.register', compact('upline'));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|string|max:255|min:3',
            'lastname' => 'required|string|max:255',
            'email' => 'required|email|string|max:255|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'accepted' => 'required|accepted',
            'upline' => 'required'
        ]);

        $uplineUser = $this->hasUpline($request->upline);

        $username = $this->username($request->firstname);

        $user = $this->storeEntity($username, $request);

        // store genealogy
        $uplineUser->downlines()->create(['user_id' => $user->id]);

        Auth::login($user);

        event(new Registered($user));

        return redirect(route('verification.notice', [], false));
    }


    private function hasUpline(?string $upline): User
    {
        abort_if(!$upline, 403);

        $uplineUser = User::firstWhere('username', $upline);

        abort_if(!$uplineUser || $uplineUser->downlines()->count() >= AppServiceProvider::$CHILD_TREE, 403);

        return $uplineUser;
    }


    private function storeEntity(string $username, Request $request): User
    {
        /** @var \App\Models\User */
        $user = User::create([
            'username' => $username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $user->profile()->create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname
        ]);

        return $user;
    }

    /**
     * Generate unique user id depends on username
     * 
     * @param string $value
     * @return string
     */
    private function username(string $value): string
    {
        $username = Str::lower(implode('', explode(' ', $value)));
        $user_count = User::where('username', $username)->count();
        if ($user_count > 0) {
            $username .= $user_count;
        }

        return $username;
    }
}
