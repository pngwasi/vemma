<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Income;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $incomes = Income::sum('amount');
        $users = User::count('id');
        $payments = Payment::sum('amount');

        $chart_payments = $this->queryChart(Payment::query(), 12);
        $chart_incomes = $this->queryChart(Income::query(), 12);

        return view('pages.admin.home', compact('incomes', 'users', 'payments', 'chart_payments', 'chart_incomes'));
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $model
     * @return array
     */
    public function queryChart($model, int $limit = 6): array
    {
        $datas = [
            'labels' => [],
            'data' => []
        ];
        $result = $model->select(DB::raw('YEAR(created_at) as year, MONTHNAME(created_at) as month, SUM(amount) as sum_amount'))
            ->groupByRaw('MONTHNAME(created_at)')
            ->groupByRaw('YEAR(created_at)')
            ->latest()
            ->limit($limit)
            ->get();
        foreach ($result as $row) {
            array_push($datas['labels'], "{$row->month} {$row->year}");
            array_push($datas['data'], $row->sum_amount);
        }
        return $datas;
    }
}
