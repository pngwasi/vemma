<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EncashmentMethod;
use App\Models\Prime;
use App\Models\User;
use App\Providers\ViewServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UsersController extends Controller
{

    public function builder()
    {
        return User::withTrashed();
    }

    public function index()
    {
        return view('pages.admin.users.index');
    }

    /**
     * @param string $username
     * @return User
     */
    public function user(string $username)
    {
        return $this->builder()->where('username', $username)->firstOrFail();
    }

    public function show(string $username)
    {
        $user = $this->user($username);

        $methods = $user->encashmentMethods()->get();

        return view('pages.admin.users.show', compact('user', 'methods'));
    }

    public function storeMethod(Request $request, string $username)
    {
        $request->validate([
            'method' => ['required', 'min:2', 'max:255', 'string'],
            'account_number' => ['required', 'min:2', 'max:255', 'string'],
        ]);

        $user = $this->user($username);

        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        $methods = $user->encashmentMethods()->count('id');
        if ($methods >= 5) {
            return back()
                ->withErrors(['method' => trans('Vous ne pouvez pas enregistrer plus de 5 méthodes')])
                ->withInput();
        }

        $user->encashmentMethods()->create([
            'method' => $request->method,
            'account_number' => $request->account_number,
            'currency' => ViewServiceProvider::CURRENCY,
        ]);

        return back()->with('created', trans("Méthode enregistrée avec succès."));
    }


    public function deleteMethod($id)
    {
        EncashmentMethod::destroy($id);

        return back();
    }


    public function userBonus(User $user)
    {
        $userPrimes = $user->primes()->latest()->get();
        $primesDownlines = $userPrimes->map(fn ($p) => intval($p->downlines))->all();

        $userDownlines = intval($user->downlinesCount ? $user->downlinesCount->count : 0);

        $availables = [];

        Prime::all()
            ->each(function (Prime $prime) use ($primesDownlines, $userDownlines, &$availables) {
                if (
                    !in_array(intval($prime->downlines), $primesDownlines) &&
                    intval($prime->downlines) <= $userDownlines
                ) {
                    $availables[] = $prime;
                }
            });

        return ['saved' => $userPrimes, 'availables' => $availables];
    }

    public function storeUserBonus(User $user, Prime $prime)
    {
        $user->primes()->create([
            'prime_id' => $prime->id,
            'downlines' => $prime->downlines,
            'label' => $prime->label
        ]);

        return $this->userBonus($user);
    }
}
