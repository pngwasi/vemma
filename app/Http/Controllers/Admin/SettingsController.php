<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index()
    {
        return view('pages.admin.settings');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255|min:3',
            'email' => 'required|email|string|max:255|unique:admins',
            'password' => 'required|string|confirmed|min:6',
        ]);

        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return back(201)->with('created', trans("L'utilisateur a été créé avec succès"));
    }


    public function updatePassword(Request $request, Admin $admin)
    {
        $request->validate([
            'password' => 'required|string|confirmed|min:6'
        ]);

        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return response()
                ->json(['errors' => ['password' => $response->message()]], 422);
        }

        if ($admin->id == $request->user()->id) {
            return response()
                ->json([
                    'errors' => [
                        'password' => trans("Vous ne pouvez pas modifier le mot de passe de l'utilisateur actuellement connecté.")
                    ]
                ], 422);
        }

        $admin->fill(['password' => Hash::make($request->password)])->save();

        return ['message' => trans('Mot de passe changé avec succès')];
    }
}
