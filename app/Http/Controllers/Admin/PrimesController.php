<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advantage;
use App\Models\Prime;
use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PrimesController extends Controller
{
    public function index()
    {
        return view('pages.admin.primes', [
            'primes' => Prime::all(),
            'advantages' => Advantage::all(),
        ]);
    }

    public function storePrime(Request $request)
    {
        $childTree = AppServiceProvider::$CHILD_TREE + 1;

        $data = $request->validate([
            'downlines' => ['required', 'numeric', 'min:' . $childTree, 'unique:primes'],
            'label' => ['required', 'string', 'min:1', 'max:50', 'unique:primes'],
        ]);

        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        Prime::create($data);

        return back(201)->with('created',  trans("Bonus créé avec succès"));
    }

    public function destroyPrime($id)
    {
        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        Prime::destroy($id);

        return back();
    }


    public function storeAdvantage(Request $request)
    {
        $childTree = AppServiceProvider::$CHILD_TREE + 1;

        $data = $request->validate([
            'downlines' => ['required', 'numeric', 'min:' . $childTree, 'unique:advantages'],
            'amount' => ['required', 'numeric', 'min:00.1', 'unique:advantages'],
        ]);

        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        Advantage::create($data);

        return back(201)->with('created',  trans("Avangate créé avec succès"));
    }

    public function destroyAdvantage($id)
    {
        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        Advantage::destroy($id);

        return back();
    }
}
