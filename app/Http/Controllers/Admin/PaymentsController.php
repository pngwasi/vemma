<?php

namespace App\Http\Controllers\Admin;

use App\Events\Payment\UserPayEvent;
use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use App\Models\Setting;
use App\Models\User;
use App\Repositories\SettingsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PaymentsController extends Controller
{
    public function index()
    {
        $methods = PaymentMethod::all();

        return view('pages.admin.payments', [
            'setting' => SettingsRepository::getSetting(),
            'methods' => $methods
        ]);
    }


    private function errors(string $message)
    {
        return response()
            ->json([
                'errors' => [
                    'amount' => trans($message)
                ]
            ], 422);
    }

    public function store(User $user)
    {
        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return $this->errors($response->message());
        }

        if ($user->payment) {
            return $this->errors("Le paiement de l'utilisateur a déjà été enregistré.");
        }

        $setting = SettingsRepository::getSetting();

        if (!$setting || $setting && !$setting->user_payment) {
            return $this->errors("Aucun paramètre n'a été défini pour les enregistrements de paie.");
        }

        $this->storePay($user, $setting);

        return response()
            ->json(['message' => trans("Le paiement de l'utilisateur a été effectué")], 201);
    }


    private function storePay(User $user, Setting $setting)
    {
        $payment = $user->payment()->create([
            'amount' => $setting->user_payment,
            'user_percentage' => $setting->user_percentage,
            'confirmed' => true,
        ]);

        $user->fill(['active' => true])->save();

        if (!$user->root) {
            event(new UserPayEvent($payment));
        }
    }


    public function storeOptions(Request $request)
    {
        $data = $request->validate([
            'user_payment' => ['required', 'min:1', 'numeric'],
            'user_percentage' => ['required', 'min:1', 'max:100', 'numeric']
        ]);

        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        $setting = SettingsRepository::getSetting();

        if ($setting) {
            $setting->fill($data)->save();
        } else {
            Setting::create($data);
        }

        return back()->with('updated', trans('Les paramètres ont été mis à jour avec succès'));
    }


    public function storeMethod(Request $request)
    {
        $request->validate([
            'method' => ['required', 'min:2', 'max:255', 'string']
        ]);

        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        $methods = PaymentMethod::count('id');
        if ($methods >= 5) {
            return back()
                ->withErrors(['method' => trans('Vous ne pouvez pas enregistrer plus de 5 méthodes')])
                ->withInput();
        }

        PaymentMethod::create([
            'method' => $request->method,
            'public' => true
        ]);

        return back()->with('created', trans("Méthode enregistrée avec succès."));
    }

    public function deleteMethod($id)
    {
        $response = Gate::inspect('super-admin');

        if (!$response->allowed()) {
            return back()->with('error', $response->message());
        }

        PaymentMethod::destroy($id);

        return back();
    }
}
