<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.admin.profile', ['auth' => $request->user()]);
    }


    public function update(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'min:3', 'max:255']
        ]);

        $auth = $request->user();

        $auth->fill($data)->save();

        return back()->with('updated', trans('Votre profil a été mis à jour avec succés'));
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'max:255', 'password:admin'],
            'new_password' => ['required', 'string', 'max:255', 'min:6', 'confirmed'],
        ]);

        $auth = $request->user();

        $auth->fill([
            'password' => Hash::make($request->new_password)
        ])->save();

        return back()->with('updated', trans("Mot de passe changé avec succès"));
    }
}
