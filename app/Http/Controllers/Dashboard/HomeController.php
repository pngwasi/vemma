<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\SettingsRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        /** @var \App\Models\User */
        $user = $request->user();

        return view('pages.dashboard.home', [
            'available_balance' => $user->availableBalance(),
            'encashment' => $user->amountEncashment(),
            'balance' => $user->amountBalance(),
            'profile' => $user->profile,
            'auth' => $user,
            'setting' => SettingsRepository::getSetting()
        ]);
    }
}
