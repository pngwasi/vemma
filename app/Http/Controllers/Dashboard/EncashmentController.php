<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\EncashmentMethod;
use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EncashmentController extends Controller
{
    public function index(Request $request)
    {
        $auth = $request->user();

        return view('pages.dashboard.encashment', [
            'balance' => $auth->availableBalance(),
            'methods' => $auth->encashmentMethods,
            'encashments' => $auth->encashments,
            'encashmentsTotal' => $auth->amountEncashment()
        ]);
    }

    private function errors(string $input, string $message)
    {
        return back()
            ->withErrors([$input => trans($message)])
            ->withInput();
    }

    public function store(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'numeric', 'min:1'],
            'method' => ['required', 'string']
        ]);

        $user = $request->user();

        if (!$user->active) {
            return $this->errors('amount', "Vous devez activer votre compte avant de continuer.");
        }

        $supported = $this->supportedMethod($request->method);

        $method = $user->encashmentMethods()->where('method', $request->method)->first();

        if (!$supported || !$method) {
            return $this->errors('method', "La méthode utilisée n'est pas supportée.");
        }

        $balance = $user->availableBalance();

        if (doubleval($request->amount) > $balance) {
            return $this->errors('amount', "Votre solde est insuffisant par rapport au montant saisi.");
        }

        $this->encashments($user, $method, doubleval($request->amout));

        return back()
            ->with('proceed', trans("Votre demande a été exécutée avec succès"));
    }


    private function encashments(User $user, EncashmentMethod $method, float $amount)
    {
        $encashment = $user->encashments()->create([
            'amount' => $amount,
            'confirmed' => true,
            'encashment_method_id' => $method->id
        ]);

        $reference = strval($encashment->id);
        $length = 11 - strlen($reference);

        if ($length > 0) {
            $reference = Str::random($length) . $reference;
        }

        $encashment->reference = $reference;
        $encashment->save();
    }

    private function supportedMethod(string $method): bool
    {
        return PaymentMethod::where('method', $method)->exists();
    }
}
