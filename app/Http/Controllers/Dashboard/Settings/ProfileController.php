<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Files\File;
use App\Http\Controllers\Controller;
use App\Rules\ValidPhone;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware(['optimizeImages:250,90'])->only('uploadImage');
    }

    public function index(Request $request)
    {
        $auth = $request->user();

        return view('pages.dashboard.settings.profile', [
            'auth' => $auth,
            'profile' => $auth->profile
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'firstname' => ['required', 'string', 'max:255', 'min:3'],
            'lastname' => ['required', 'string', 'max:255'],
            'middlename' => ['nullable', 'string', 'max:255'],
            'gender' => ['nullable', 'string', 'max:255', 'min:2'],
            'phone' => ['required', 'string', 'max:50', new ValidPhone],
            'birthdate' => ['required', 'string', 'date'],
            'tax_identification_number' => ['nullable', 'max:255'],
            'address' => ['nullable', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'region' => ['required', 'string', 'max:255'],
            'description' => ['nullable', 'string', 'max:400', 'min:15']
        ]);

        $auth = $request->user();

        $auth->profile->fill($data)->save();

        return back()->with('updated', trans('Votre profil a été mis à jour avec succés'));
    }


    public function uploadImage(Request $request)
    {
        $request->validate([
            'image' => File::IMAGE_RULES
        ]);

        $auth = $request->user();
        $file = $request->file('image');

        $auth->profile->fill([
            'image' => $file->storeAs(File::IMAGES_PATH, "{$auth->username}.jpg")
        ])->save();

        return back()->with('updated', trans("Image profil téléchargé avec succès"));
    }
}
