<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function index()
    {
        return view('pages.dashboard.settings.change-password');
    }

    public function update(Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'max:255', 'password:web'],
            'new_password' => ['required', 'string', 'max:255', 'min:6', 'confirmed'],
        ]);

        $auth = $request->user();

        $auth->fill([
            'password' => Hash::make($request->new_password)
        ])->save();

        return back()->with('updated', trans("Mot de passe changé avec succès"));
    }
}
