<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\GenealogyRepository;
use Illuminate\Http\Request;

class GenealogyController extends Controller
{
    public function index(Request $request)
    {

        $username = $request->query('username');
        $auth = $username ? User::firstWhere('username', $username) : $request->user();

        $genealogies = (new GenealogyRepository)->getConstructedTree($auth);

        $genealogiesChart = collect($genealogies)
            ->map(fn ($item) => $item ? (object) [
                'username' => $item->username,
                'fullname' => $item->fullname,
                'id' => $item->id
            ] : $item);

        return view('pages.dashboard.genealogy', compact('genealogies', 'genealogiesChart'));
    }
}
