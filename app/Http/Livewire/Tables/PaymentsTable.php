<?php

namespace App\Http\Livewire\Tables;

use App\Models\Payment;
use App\Models\User;
use App\Providers\ViewServiceProvider;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;


class PaymentsTable extends LivewireDatatable
{
    public $model = Payment::class;

    public $sort = 'desc';

    public function columns()
    {
        return [
            NumberColumn::name('id'),

            Column::name('user.username')
                ->searchable()
                ->label(trans("Nom d'utilisateur")),

            Column::name('amount')
                ->label(trans("Montant") . "(" . ViewServiceProvider::CURRENCY . ")"),

            BooleanColumn::name('confirmed')
                ->label(trans("Approuvé")),

            Column::callback(['created_at'], fn () => ViewServiceProvider::CURRENCY)
                ->label(trans("Devise")),

            DateColumn::name('created_at')
                ->filterable()
                ->label(trans('Ajouté le')),
        ];
    }
}
