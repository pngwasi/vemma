<?php

namespace App\Http\Livewire\Tables;

use App\Models\User;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;


class UsersTable extends LivewireDatatable
{
    public $sort = 'desc';

    public function builder()
    {
        return User::withTrashed();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id'),

            Column::name('username')
                ->searchable()
                ->label(trans("Nom d'utilisateur")),

            Column::name('email')
                ->searchable()
                ->label(trans("Adresse e-mail")),

            BooleanColumn::name('active')
                ->alignCenter()
                ->label(trans("Compte activé")),

            DateColumn::name('created_at')
                ->label(trans('Ajouté le')),

            Column::callback(
                ['username'],
                fn ($username) =>
                view('livewire.tables.actions.show-route', ['route' => route('admin.users.show', compact('username'), false)])
            )->label(trans('Profil'))
                ->alignCenter(),

            Column::callback(['id'], function ($id) {
                $user = $this->builder()->find($id);

                return view('livewire.admin.users.btn-genealogy', [
                    'datas' => [
                        'username' => $user->username,
                        'genealogies' => $user->genealogy()
                    ]
                ]);
            })->label(trans('Généalogie'))
                ->alignCenter(),

            BooleanColumn::name('auth')
                ->label(trans('Actif')),

            Column::callback(['id', 'created_at'], function ($id) {
                return view('livewire.admin.users.btn-delete', ['user' => $this->builder()->find($id)]);
            })
        ];
    }

    public function delete($id)
    {
        $user = $this->builder()->find($id);
        $user->auth = !$user->auth;
        $user->save();
    }
}
