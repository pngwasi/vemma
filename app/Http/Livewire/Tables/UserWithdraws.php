<?php

namespace App\Http\Livewire\Tables;

use App\Models\User;
use App\Providers\ViewServiceProvider;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;


class UserWithdraws extends LivewireDatatable
{
    public $sort = 'desc';

    public function builder()
    {
        /** @var \App\Models\User */
        $user = User::query()->findOrFail($this->params['user'] ?? null);

        return $user->encashments()->getQuery();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id'),

            Column::name('reference')
                ->searchable()
                ->label("#" . trans("Référence")),

            Column::name('amount')
                ->label(trans("Montant") . "(" . ViewServiceProvider::CURRENCY . ")"),

            Column::name('cross')
                ->label(trans("Brute") . "(" . ViewServiceProvider::CURRENCY . ")"),

            Column::name('net')
                ->label(trans("Net") . "(" . ViewServiceProvider::CURRENCY . ")"),

            BooleanColumn::name('confirmed')
                ->label(trans("Approuvé")),

            Column::name('encashment_method.method')
                ->label(trans("Méthode")),

            Column::name('encashment_method.account_number')
                ->label(trans("Numéro de compte")),

            Column::name('encashment_method.currency')
                ->label(trans("Devise")),

            DateColumn::name('created_at')
                ->filterable()
                ->label(trans('Ajouté le')),
        ];
    }
}
