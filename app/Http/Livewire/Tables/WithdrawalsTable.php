<?php

namespace App\Http\Livewire\Tables;

use App\Models\Encashment;
use App\Providers\ViewServiceProvider;
use Illuminate\Support\Facades\Gate;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;

class WithdrawalsTable extends LivewireDatatable
{

    public $model = Encashment::class;

    public $sort = 'desc';


    public function columns()
    {
        return [
            NumberColumn::name('id'),

            Column::name('user.username')
                ->searchable()
                ->label(trans("Nom d'utilisateur")),

            Column::name('reference')
                ->searchable()
                ->label("#" . trans("Référence")),

            Column::name('amount')
                ->label(trans("Montant") . "(" . ViewServiceProvider::CURRENCY . ")"),

            Column::name('cross')
                ->label(trans("Brute") . "(" . ViewServiceProvider::CURRENCY . ")"),

            Column::name('net')
                ->label(trans("Net") . "(" . ViewServiceProvider::CURRENCY . ")"),

            BooleanColumn::name('confirmed')
                ->label(trans("Approuvé")),

            Column::callback(['id'], function ($id) {
                $withdraw = $this->model::find($id);

                $user = $withdraw->user;

                $approved = $withdraw->confirmed ?
                    trans('Approuvé') : view('livewire.admin.withdrawals.btn-approve', [
                        'withdraw' => $withdraw
                    ]);

                return $user->active ? $approved : trans("Compte utilisateur inactif");
            })->label(trans("Statut")),

            Column::name('encashment_method.method')
                ->label(trans("Méthode")),

            Column::name('encashment_method.account_number')
                ->label(trans("Numéro de compte")),

            Column::name('encashment_method.currency')
                ->label(trans("Devise")),

            DateColumn::name('created_at')
                ->filterable()
                ->label(trans('Ajouté le')),
        ];
    }

    public function approve($id)
    {
        $withdraw = $this->model::find($id);

        $response = Gate::inspect('super-admin');

        if ($response->allowed()) {
            $withdraw->confirmed = true;
            $withdraw->save();
        }
    }
}
