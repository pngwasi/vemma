<?php

namespace App\Http\Livewire\Tables;

use App\Models\Income;
use App\Providers\ViewServiceProvider;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;


class IncomesTable extends LivewireDatatable
{
    public $model = Income::class;

    public $sort = 'desc';

    public function columns()
    {
        return [
            NumberColumn::name('id'),

            Column::name('user.username')
                ->label(trans("Nom d'utilisateur (Upline)")),

            Column::name('amount')
                ->label(trans("Montant") . "(" . ViewServiceProvider::CURRENCY . ")"),

            Column::name('winning_percentage')
                ->label(trans("Revenu pourcentage(%)")),

            Column::name('percentage')
                ->label(trans("Pourcentage d'utilisateur(%)")),

            Column::name('cross')
                ->label(trans("Total") . "(" . ViewServiceProvider::CURRENCY . ")"),

            DateColumn::name('created_at')
                ->filterable()
                ->label(trans('Ajouté le')),
        ];
    }
}
