<?php

namespace App\Http\Livewire\Tables;

use App\Models\Admin;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;

class AdminsTable extends LivewireDatatable
{
    public $model = Admin::class;

    public $sort = 'desc';


    public function columns()
    {
        return [
            NumberColumn::name('id'),

            Column::name('name')
                ->searchable()
                ->editable()
                ->label(trans("Nom")),

            Column::name('email')
                ->searchable()
                ->label(trans("Email")),

            BooleanColumn::name('super')
                ->label(trans("Super Admin")),

            DateColumn::name('created_at')
                ->label(trans('Ajouté le')),

            Column::callback(['id'], function ($id) {
                return view('livewire.admin.settings.btn-edit-admin', [
                    'admin' => Admin::find($id)
                ]);
            })->label(trans('Editer')),

            Column::delete()
                ->alignCenter()
                ->label(trans('Supprimer')),
        ];
    }

    public function delete($id)
    {
        $admin  = Admin::find($id);
        if (!$admin->super) {
            $admin->delete();
        }
    }
}
