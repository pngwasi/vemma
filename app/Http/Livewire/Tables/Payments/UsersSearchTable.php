<?php

namespace App\Http\Livewire\Tables\Payments;

use App\Http\Livewire\Searchable\DatatableUserSearch;
use App\Providers\ViewServiceProvider;
use Mediconesystems\LivewireDatatables\Column;
use App\Repositories\SettingsRepository;

class UsersSearchTable extends DatatableUserSearch
{
    public function addColums(): array
    {
        return [
            Column::callback(['id'], function ($id) {
                $user = $this->builder()->find($id);

                return $user->payment ? trans('Déjà payé') : view('livewire.admin.payments.btn-payments', [
                    'datas' => [
                        'user' => $user,
                        'payment' => $user->payment,
                        'setting' => SettingsRepository::getSetting(),
                        'currency' => ViewServiceProvider::CURRENCY
                    ]
                ]);
            })->label(trans('Payer'))
        ];
    }
}
