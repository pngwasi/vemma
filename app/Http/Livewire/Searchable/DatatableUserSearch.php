<?php

namespace App\Http\Livewire\Searchable;

use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class DatatableUserSearch extends LivewireDatatable
{
    public $sort = 'desc';

    public $perPage = 1;

    public function builder()
    {
        return User::withTrashed();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id'),

            Column::name('username')
                ->searchable()
                ->label(trans("Nom d'utilisateur")),

            Column::name('email')
                ->searchable()
                ->label(trans("Adresse e-mail")),

            ...$this->addColums()
        ];
    }

    public function addColums(): array
    {
        return [];
    }
}
