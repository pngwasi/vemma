<?php

namespace App\Http\Middleware\Admin;

use App\Http\Middleware\Authenticate as MainAuthentication;


class Authenticate extends MainAuthentication
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('admin.login');
        }
    }
}
