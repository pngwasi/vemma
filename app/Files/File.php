<?php

namespace App\Files;

class File
{
    /**
     * @var int SIZE
     */
    public const SIZE = 1024;

    /**
     * @var array
     */
    public const IMAGE_RULES = ['required', 'image', "max:" . (self::SIZE * 5) . "", "mimes:jpeg,png"];


    public const IMAGE_RULES_OPTIONAL = ['nullable', 'image', "max:" . (self::SIZE * 5) . "", "mimes:jpeg,png"];

    /**
     * @var string EVENTS_COVERS_PATH
     */
    public const IMAGES_PATH = 'images';
}
