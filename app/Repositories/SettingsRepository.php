<?php

namespace App\Repositories;

use App\Models\Setting;

class SettingsRepository
{
    public static function getSetting(): ?Setting
    {
        return Setting::latest()->first();
    }
}
