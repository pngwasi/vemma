<?php

namespace App\Repositories;

use App\Models\Genealogy;
use App\Models\User;
use App\Providers\AppServiceProvider;

class GenealogyRepository
{
    private $downlines = [];

    public function __construct()
    {
    }


    private function downlines(User $user)
    {
        return $user->downlines()
            ->get()
            ->map(fn (Genealogy $u) => $u->userDownline);
    }


    private function dataTree(?User $user, int $child)
    {
        $downlines = !$user ? [] : $this->downlines($user)->all();
        $datas = [];
        for ($i = 0; $i < $child; $i++) {
            array_push($datas, $downlines[$i] ?? null);
        }
        return $datas;
    }


    public function constructDataTree(?User $user, int $tree, int $child)
    {
        $datas = $this->dataTree($user, $child);

        array_push($this->downlines, $user, ...$datas);

        for ($i = 0; $i < ($tree - 1); $i++) {
            $pendings = [];
            foreach ($datas as $value) {
                $rdatas = $this->dataTree($value, $child);
                array_push($pendings, ...$rdatas);
            }
            array_push($this->downlines, ...$pendings);
            $datas = $pendings;
        }
    }


    public function getConstructedTree(?User $user): array
    {
        $this->downlines = [];

        $this->constructDataTree($user, 2, AppServiceProvider::$CHILD_TREE);

        return $this->downlines;
    }
}
