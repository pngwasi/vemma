<?php

namespace App\Events\Payment;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserPayEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $payment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }


    public function getUser(): User
    {
        return $this->payment->user;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getUpline(): User
    {
        return $this->payment->user->userUpline;
    }
}
