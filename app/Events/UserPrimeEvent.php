<?php

namespace App\Events;

use App\Models\Prime;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserPrimeEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $user;


    private $prime;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Prime $prime)
    {
        $this->user = $user;
        $this->prime = $prime;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getPrime(): Prime
    {
        return $this->prime;
    }
}
