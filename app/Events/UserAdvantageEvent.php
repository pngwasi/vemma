<?php

namespace App\Events;

use App\Models\Advantage;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserAdvantageEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $advantage;

    private $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Advantage $advantage)
    {
        $this->user = $user;
        $this->advantage = $advantage;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getAdvantage(): Advantage
    {
        return $this->advantage;
    }
}
