<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPrime extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prime_id',
        'downlines',
        'label'
    ];


    public function prime()
    {
        $this->belongsTo(Prime::class);
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
