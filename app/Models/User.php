<?php

namespace App\Models;

use App\Providers\AppServiceProvider;
use App\Repositories\GenealogyRepository;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['fullname', 'image'];


    /**
     * Get the file path.
     *
     * @return string
     */
    public function getImageAttribute()
    {
        $image = $this->profile->image;

        return $image ? Storage::url($image) : null;
    }

    public function getFullnameAttribute()
    {
        return Str::title("{$this->profile->firstname} {$this->profile->lastname}");
    }

    public function getDownlinesTotalAttribute()
    {
        return $this->downlinesCount ? $this->downlinesCount->count : 0;
    }

    public function getUserUplineAttribute()
    {
        return $this->upline ? $this->upline->userUpline : null;
    }

    public function getUserDownlinesAttribute(): Collection
    {
        return $this->downlines()->get()->map(fn (Genealogy $u) => $u->userDownline);
    }

    public function downlinesPad(): Collection
    {
        return $this->user_downlines->pad(AppServiceProvider::$CHILD_TREE, null);
    }

    public function hasCompletedTree(): bool
    {
        return  $this->downlinesPad()->every(fn ($u) => $u && $u->payment);
    }

    public function genealogy(): array
    {
        return (new GenealogyRepository)
            ->getConstructedTree($this);
    }


    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function downlines()
    {
        return $this->hasMany(Genealogy::class, 'upline_id');
    }

    public function downlinesCount()
    {
        return $this->hasOne(DownlinesCount::class);
    }

    public function primes()
    {
        return $this->hasMany(UserPrime::class);
    }

    public function upline()
    {
        return $this->hasOne(Genealogy::class);
    }

    public function balances()
    {
        return $this->hasMany(Balance::class);
    }

    public function pendingBalances()
    {
        return $this->hasMany(PendingBalance::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function encashmentMethods()
    {
        return $this->hasMany(EncashmentMethod::class);
    }

    public function encashments()
    {
        return $this->hasMany(Encashment::class);
    }

    public function availableBalance()
    {
        return round($this->balances()->sum('amount') - $this->encashments()->sum('amount'), 2);
    }

    public function amountEncashment()
    {
        return round($this->encashments()->sum('amount'), 2);
    }

    public function amountBalance()
    {
        return round($this->balances()->sum('amount'), 2);
    }
}
