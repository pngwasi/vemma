<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'middlename',
        'gender',
        'phone',
        'image',
        'birthdate',
        'tax_identification_number',
        'address',
        'country',
        'region',
        'description'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
