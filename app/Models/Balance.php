<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'confirmed',
        'amount',
        'percentage',
        'cross'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
