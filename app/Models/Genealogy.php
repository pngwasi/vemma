<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genealogy extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
        'user_id'
    ];


    public function userUpline()
    {
        return $this->belongsTo(User::class, 'upline_id');
    }

    public function userDownline()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
