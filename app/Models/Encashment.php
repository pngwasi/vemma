<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Encashment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference',
        'amount',
        'cross',
        'net',
        'confirmed',
        'encashment_method_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function encashmentMethod()
    {
        return $this->belongsTo(EncashmentMethod::class);
    }
}
