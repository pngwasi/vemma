<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * genealogy child tree - default value: 3
     *
     * @var int
     */
    public static int $CHILD_TREE;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        self::$CHILD_TREE = intval(env('VEMMA_CHILD_TREE', 3));
        View::share('childTree', self::$CHILD_TREE);
    }
}
