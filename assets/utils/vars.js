export const modalItemsUpdatedEvent = "modalItemsUpdated"

export const openModalEvent = "openModal"

export const routeFromChildEvent = "routeFromChild"
