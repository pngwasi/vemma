import Modal from 'bootstrap/js/dist/modal'
import { capitalize, eventListenOne } from '@root/functions/functions'
import { modalItemsUpdatedEvent, openModalEvent } from '@root/utils/vars'
import { innerDump, Localize, spinnerHtml } from './dom'


let modalId = null
export const openModal = (data, parentEvent = null) => {
    const modal = modalId ? document.getElementById(modalId) : null
    if (modal) {
        innerDump(modal.querySelector('.modal-body'), data)

        if (!modal.classList.contains('show')) {
            const imodal = new Modal(modal)
            imodal.show()
        }
        return
    }
    const html = /*html*/`<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-body pb-2">${data}</div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-primary btn-sm text-white"
                            data-bs-dismiss="modal">${Localize({ fr: 'Fermer', en: 'Close' })}</button>
                    </div>
                </div>
            </div>
        </div>`

    modalId = `mo-${Math.random()}`

    const doc = document.createRange().createContextualFragment(html)
    const el = doc.firstElementChild

    el.id = modalId;
    document.body.appendChild(el)

    const imodal = new Modal(el)
    imodal.show()

    if (parentEvent) {
        const cb = () => window.dispatchEvent(new Event(`${parentEvent}:destroyed`))
        el.addEventListener('hide.bs.modal', cb)
    }
}


export const openModalEventFrame = (types, fn) => {

    const memo = {};

    const handleEvent = (e) => {
        let { type, datas, id } = e.detail

        if (id && memo[id]) {
            datas = memo[id]
        } else if (id && !memo[id]) {
            memo[id] = datas
        }

        const content = fn(datas)
        if (type == types && content) {
            openModal(content, types)

            window.dispatchEvent(new Event(type))

            eventListenOne(null, modalItemsUpdatedEvent, ({ detail: { type: itype, data: idata } }) => {

                let finaldata = id ? memo[id] : datas

                switch (itype) {
                    case "add":
                        if (Array.isArray(finaldata.data)) {
                            finaldata.data = [idata, ...finaldata]
                        } else {
                            finaldata.data = idata
                        }
                        break;

                    case "put":
                        finaldata.data = idata
                        break;
                    case "remove":
                        if (Array.isArray(finaldata.data)) {
                            finaldata.data = finaldata.data.filter(e => e.id != idata.id)
                        } else {
                            finaldata.data = null
                        }
                        break;
                    default:
                        break;
                }

                if (memo[id]) {
                    memo[id] = finaldata
                }

                window.dispatchEvent(
                    new CustomEvent(openModalEvent, { detail: { type, finaldata, id } })
                )
            })
        }
    }

    window.addEventListener(openModalEvent, handleEvent)


    // on listen when the modal is closed and destroy it by raise callback
    let onModalDestroyedCb = null
    const modalDestroyedEvent = `${types}:destroyed`;

    const actionsOrdestroy = {
        destroy() {
            window.removeEventListener(openModalEvent, handleEvent)
            onModalDestroyedCb && window.removeEventListener(modalDestroyedEvent, onModalDestroyedCb)
        },
        onDestroyed(callback = null) {
            if (!callback) return
            onModalDestroyedCb = callback
            window.addEventListener(modalDestroyedEvent, callback)
        },
        onContentMounted(callback = null) {
            callback && eventListenOne(null, types, callback)
        },
        onItemEvent(parentId, callback) {
            eventListenOne(null, types, (e) => {
                const el = document.getElementById(parentId)
                Array.from(el.querySelectorAll('.action--js'))
                    .forEach(el => el.addEventListener('click', callback.bind(undefined, el)))
            })
        },
        dispatchEventUpdatedItems(data, more = {}) {
            window.dispatchEvent(new CustomEvent(modalItemsUpdatedEvent, {
                detail: { types, data, ...more }
            }))
        },
        container(content = '', title = null, subtitle = '', spinner = false) {
            const parentId = Math.random() * 100 ** 9;
            return {
                parentId,
                contentElement() {
                    return document.getElementById(`${parentId}`)
                        .querySelector('[data-content]')
                },
                content: /*html*/`<div id="${parentId}">
                    ${title || subtitle ? /*html*/`
                        <div class="d-flex justify-content-between">
                            ${htmlTitleModal(title || '')}
                            <span>${subtitle || ''}</span>
                        </div>
                    `: ''}
                    <div data-content>
                        ${spinner ? spinnerHtml() : ''}
                        ${content}
                    </div>
                </div>`
            }
        }
    }

    return actionsOrdestroy
}

export function htmlModalButton(id, text = "", color = "btn-primary") {
    return /*html*/`<button data-id="${id}" class="btn action--js text-xs ${color} p-1 text-white">${text}</button>`
}

export function htmlTitleModal(text) {
    return /*html*/`<div class="my-3"><h5>${capitalize(text)}</h5></div>`
}

export function htmlAlterModal(text, color = 'alert-info') {
    return  /*html*/`<div class="alert ${color}" role="alert">${text}</div>`
}
