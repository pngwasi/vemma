
/**
 * @param { HTMLElement | EventTarget } target
 * @returns { HTMLButtonElement }
*/
// @ts-ignore
export const FormBtn = (target) => target ? target.querySelector('button[type="submit"]') : null

export const clearInput = (parent) => {
    const inptus = parent.querySelectorAll('input[type="text"]');
    const textarea = parent.querySelectorAll('textarea');
    const emails = parent.querySelectorAll('input[type="email"]');
    const tels = parent.querySelectorAll('input[type="tel"]');
    ([...inptus, ...textarea, ...emails, ...tels]).forEach(element => element.value = '')
}


/**
 * @param { Object } trans 
 * @param { string } defaults
 * @returns { string }
 */
export const Localize = (trans = {}, defaults = 'fr') => {
    const lang = document.querySelector('html').lang
    return trans[lang] ? trans[lang] : trans[defaults]
}

/**
 * @param { HTMLElement | Element } parent 
 */
export const removeChilds = (parent) => {
    Array.from(parent.children)
        .forEach(element => parent.removeChild(element))
}


export const innerDump = (parent, data) => {
    removeChilds(parent)
    parent.innerHTML = ''
    parent.appendChild(document.createRange().createContextualFragment(data))
}


export const Btn = {
    btns: [],
    /**
     * @param { HTMLButtonElement } element 
     */
    loading(element) {
        if (!element) return
        const el = {
            element: element,
            html: element.innerHTML,
            text: element.innerText
        }
        this.btns.unshift(el)
        element.disabled = true
        element.innerHTML = /*html*/`
            <div class="d-flex justify-content-center align-items-center"> 
                <span>${el.html}</span>
                <span class="mx-1"></span>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span class="visually-hidden">Loading...</span>
            <div/>
        `
    },
    hide() {
        const h = this.btns
        if (h.length) {
            const n = (h[h.length - 1])
            const el = this.get(n.element)
            if (el) {
                el.disabled = false
                removeChilds(el)
                el.innerHTML = n.html
            }

            this.btns = this.btns.slice(0, h.length - 1)
        }
    },
    /**
     * @returns { HTMLButtonElement }
     */
    get(v) {
        return v
    },
}


export const HtmlAlert = {
    parents: [],

    message(message) {
        if (message !== null || message !== undefined) {
            if (typeof message === 'string')
                return message;
            if (Array.isArray(message))
                return this.fromArray(message);
            if (message.message && !message.errors)
                return message.message;
            if (message.message && message.errors)
                return this.fromObject(message.errors);
        } else {
            return Localize({
                fr: 'Une erreur est survenue en interne, veuillez réessayer plus tard',
                en: 'An error has occurred internally, please try again later'
            })
        }
    },

    /**
     * @param { object } object 
     * @returns { string }
     */
    fromObject(object) {
        return Object.keys(object).map(key => object[key].join(".<br/>")).join("<br/>")
    },
    /**
     * @param { Array<string> } arr 
     */
    fromArray(arr) {
        return arr.join(".<br/>")
    }
}

export const spinnerHtml = () => {
    return /*html*/`
    <div class="d-flex justify-content-center align-items-center"> 
        <div class="spinner-border text-primary" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </div>
    `
}