export function requireControllerContext(modules) {
    const context = function (key) {
        return context.modules[key]
    }
    context.modules = {}
    for (const key in modules) {
        const newKey = key.replace(/.\/controllers\//, '')
        context.modules[newKey] = modules[key]
    }
    context.keys = function () {
        return Object.keys(this.modules)
    }

    return context
}