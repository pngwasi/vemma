import { render, unmountComponentAtNode } from 'react-dom'

/**
 * 
 * @param { JSX.Element } App 
 * @param { HTMLElement|Element } element 
 */
const ReactInstance = (App, element) => {
    render(App, element)
    return () => unmountComponentAtNode(element)
}


export default ReactInstance