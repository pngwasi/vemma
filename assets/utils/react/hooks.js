import { useEffect, useState } from "react"
import { ApiRequestAxios } from "../axios";


export const mergeRefs = (...refs) => {
    const filteredRefs = refs.filter(Boolean);
    if (!filteredRefs.length) return null;
    if (filteredRefs.length === 0) return filteredRefs[0];
    return inst => {
        for (const ref of filteredRefs) {
            if (typeof ref === 'function') {
                ref(inst);
            } else if (ref) {
                ref.current = inst;
            }
        }
    };
};


export const useErrorInput = (name, errors) => {
    const [error, setError] = useState(null)

    useEffect(() => {
        if (name in errors) {
            setError(errors[name])
        }
    }, [errors])

    const onKeyUp = () => setError(null)

    return { error, onKeyUp }
}


export const useFormValidator = () => {
    const [errors, setErrors] = useState({})
    const [success, setSuccess] = useState(null)
    const [loading, setLoading] = useState(null)


    return { errors, setErrors, success, setSuccess, loading, setLoading }
}

export const useFetch = (_loading = false) => {
    const [loading, setLoading] = useState(_loading)
    const [error, setError] = useState(null)
    const [datas, setDatas] = useState(null)
    /**
     * @param {string} method 
     * @param {string} url 
     * @param { any? } datas 
     * @param { any? } maintainLoading 
     * @returns { Promise<import('axios').AxiosResponse<any>> }
     */
    const fetchAPi = (url, method = 'get', datas = {}, maintainLoading = false) => {
        setError(null)
        setLoading(true)

        return ApiRequestAxios(url, method, datas)
            .then((res) => {
                setDatas(res.data)
                setError(null)
                return res
            })
            .catch((err) => {
                setError(err)
                maintainLoading && setLoading(false)
                return Promise.reject(err)
            })
            .finally(() => {
                !maintainLoading && setLoading(false)
            })

    }

    return { loading, error, datas, fetchAPi, ApiRequestAxios }
}