import { iframeResizer } from 'iframe-resizer'
import { routeFromChildEvent } from './vars'

// @ts-ignore
window.resizeIframe = (obj) => iframeResizer({}, obj)

window.addEventListener(routeFromChildEvent, (e) => {
    // @ts-ignore
    window.location.href = e.detail
})