import '@stimulus/polyfills'
import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import { requireControllerContext } from './utils/stimulus'
import 'bootstrap/js/dist/dropdown';
import './utils/iframeResizer'


const application = Application.start()
//@ts-ignore
const context = process.env.NODE_ENV === "development" ? requireControllerContext(import.meta.globEager('./**/*_controller.js')) : require.context("./controllers", true, /\_controller.js$/)
application.load(definitionsFromContext(context))
