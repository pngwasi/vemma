import Tab from 'bootstrap/js/dist/tab'
import { Controller } from "stimulus"

export default class extends Controller {

    connect() {
        const hash = window.location.hash || window.sessionStorage.getItem('tabs-link-hash')
        if (hash) {
            const el = this.element.querySelector(`a[href="${hash}"`)
            el && (new Tab(el)).show()
        }
        this.tabsLink()
    }

    tabsLink() {
        Array.from(this.element.querySelectorAll('.nav-link'))
            .forEach(link => link.addEventListener('click', () => {
                window.sessionStorage.setItem('tabs-link-hash', link.getAttribute('href'))
            }))
    }
}
