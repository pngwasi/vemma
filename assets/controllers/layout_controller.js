import { Controller } from "stimulus"

export default class extends Controller {

    initialize() {
        this.targets.find('collapse-toggle')
            .addEventListener('click', this.toggleSidebar)

        this.layer = document.getElementById('close-layer')
        this.layer.addEventListener('click', this.closeLayer)
    }

    connect() {
        const openShare = this.targets.find('open-share')
        openShare?.addEventListener('click', this.openShareData.bind(this))

        openShare && this.importOpenShare()
    }


    importOpenShare() {
        if (!navigator.share) {
            import('share-api-polyfill')
        }
    }


    disconnect() {
        this.targets?.find('collapse-toggle')
            .removeEventListener('click', this.toggleSidebar)

        this.layer.removeEventListener('click', this.closeLayer)

        this.targets.find('open-share')
            .removeEventListener('click', this.openShareData.bind(this))
    }


    toggleSidebar() {
        document.body.classList.toggle('sidebar-show')
    }

    closeLayer() {
        document.body.classList.remove('sidebar-show')
    }

    openShareData() {
        const el = this.targets.find('open-share')
        const url = el.getAttribute('data-url')
        const username = el.getAttribute('data-username')

        this.openShare(`@${username}`, null, url)
    }

    async openShare(title, text, url) {
        const datas = { title, text, url }
        const options = {
            language: document.querySelector('html').lang
        }
        if (navigator.share) {
            navigator.share(datas, options)
            return
        }

        this.importOpenShare()

        navigator.share(datas, options)
    }

}