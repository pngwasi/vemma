import { Controller } from "stimulus"

export default class extends Controller {

    async connect() {
    }

    onChangeImage(e) {
        this.imageDisplay.src = URL.createObjectURL(e.target.files[0]);
    }


    /**
     * @returns { HTMLImageElement }
    */
    get imageDisplay() {
        // @ts-ignore
        return this.targets.find('imageDisplay')
    }

}