import { Controller } from "stimulus"

export default class extends Controller {

    async connect() {
        this.react = (await import("@root/components/genealogy/chart-org"))
            // @ts-ignore
            .default(this.targets.find('chart-org'), window.genealogiesChart)
    }

    disconnect() {
        this.react && this.react()
    }

}