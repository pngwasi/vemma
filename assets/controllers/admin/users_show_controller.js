import { Controller } from "stimulus"

export default class extends Controller {

    async connect() {
        this.react = (await import("@root/components/genealogy/chart-org"))
            // @ts-ignore
            .default(this.targets.find('chart-org'), window.genealogiesChart, false)
        this.react1 = (await import("./users-show/bonus")).default(this.targets.find('bonus'))
    }

    disconnect() {
        this.react && this.react()
        this.react1 && this.react1()
    }

}
