import React from 'react'
import ReactInstance from '@root/utils/react/react-instance.jsx'
import FormTextControl from '@root/components/FormTextControl'
import Button from '@root/components/Button'
import { useFetch, useFormValidator } from '@root/utils/react/hooks'


const Main = ({ data }) => {
    const { errors, setErrors, setSuccess, success } = useFormValidator()
    const { fetchAPi, loading } = useFetch()

    const handleSubmit = (e) => {
        e.preventDefault()

        fetchAPi(`/vemma/admin/settings/admins/${data.id}`, 'post', new FormData(e.target))
            .then(({ data: { message } }) => {
                setSuccess(message)
                setErrors({})
            })
            .catch((error) => {
                if (error?.errors) {
                    setErrors(error.errors || {})
                }
            })
    }

    return <>
        <h6>Changer le mot de passe</h6>
        <div className="row">
            <div className="col-lg-6">
                {success && <div className="alert alert-success" role="alert">{success}</div>}
                <form autoComplete="off" onSubmit={handleSubmit} noValidate>
                    <FormTextControl errors={errors} type="password" label="Mot de passe" name="password" />
                    <FormTextControl errors={errors} type="password" label="Confirmez mot de password" name="password_confirmation" />
                    <Button text="Enregistrer" loading={loading} className="btn-sm" type="submit" />
                </form>
            </div>
        </div>
    </>
}

export default (el, data) => ReactInstance(<Main data={data} />, el)