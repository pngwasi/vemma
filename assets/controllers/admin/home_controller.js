//@ts-nocheck
import { Controller } from "stimulus"
import { ChartLine, ChartBar } from "@root/utils/chart"

export default class extends Controller {

    async connect() {
        this.chart()
    }

    async chart() {
        this.chartjs = (await import('chart.js')).default
        ChartLine(
            this.chartjs,
            this.targets.find("payments-chart"),
            window.chart_payments.labels,
            window.chart_payments.data,
        )
        ChartBar(
            this.chartjs,
            this.targets.find("incomes-chart"),
            window.chart_incomes.labels,
            window.chart_incomes.data,
        )
    }

}
