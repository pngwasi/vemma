import { openModalEventFrame } from "@root/utils/dom/openModal"
import { Controller } from "stimulus"

export default class extends Controller {

    async connect() {
        this.modal = openModalEventFrame('userPay', ({ type, data }) => {
            switch (type) {
                case 'pay':
                    return this.content(data)
            }
        })

        this.modal.onDestroyed(() => {
            this.react && this.react()
        })
    }

    content(data) {
        const container = this.modal.container('', `@${data.user.username}`, null, true)
        this.modal.onContentMounted(async () => {
            this.react = (await import('./payments/user-pay.jsx'))
                .default(container.contentElement(), data)
        })
        return container.content
    }

    disconnect() {
        this.modal && this.modal.destroy()
    }

}
