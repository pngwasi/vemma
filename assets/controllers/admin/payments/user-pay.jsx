import React from 'react'
import ReactInstance from '@root/utils/react/react-instance.jsx'
import FormTextControl from '@root/components/FormTextControl'
import Button from '@root/components/Button'
import { useFetch, useFormValidator } from '@root/utils/react/hooks'


const Main = ({ data }) => {
    const { errors, setErrors, setSuccess, success } = useFormValidator()
    const { fetchAPi, loading } = useFetch()

    const handleSubmit = (e) => {
        e.preventDefault()

        fetchAPi(`/vemma/admin/payments/${data.user.id}`, 'post', {})
            .then(({ data: { message } }) => {
                setSuccess(message)
                setErrors({})
            })
            .catch((error) => {
                if (error?.errors) {
                    setErrors(error.errors || {})
                }
            })
    }

    return <>
        <h6>Enregistrer paie</h6>
        <div className="row">
            <div className="col-lg-6">
                {success && <div className="alert alert-success" role="alert">{success}</div>}
                {
                    !data?.setting?.user_payment && (
                        <div className="alert alert-warning" role="alert">
                            Aucun paramètre n'a été défini pour les enregistrements de paie
                        </div>
                    )
                }

                {
                    data?.user?.payment && (
                        <div className="alert alert-success" role="alert">
                            Le paiement de l'utilisateur a déjà été enregistré
                        </div>
                    )
                }

                {data?.setting?.user_payment && !data?.user?.payment && !success && (
                    <form autoComplete="off" onSubmit={handleSubmit} noValidate>
                        <FormTextControl
                            errors={errors}
                            type="number"
                            defaultValue={data?.setting?.user_payment}
                            readOnly={true}
                            label={`Montant (${data.currency})`}
                            name="amount" />

                        <Button text="Enregistrer" loading={loading} className="btn-sm" type="submit" />
                    </form>
                )}
            </div>
        </div>
    </>
}

export default (el, data) => ReactInstance(<Main data={data} />, el)