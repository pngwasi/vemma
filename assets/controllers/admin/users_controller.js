import { openModalEventFrame } from "@root/utils/dom/openModal"
import { Controller } from "stimulus"

export default class extends Controller {

    connect() {
        this.modal = openModalEventFrame('usersGenealogy', ({ type, data }) => {
            switch (type) {
                case 'genealogy':
                    return this.content(data)
            }
        })

        this.modal.onDestroyed(() => {
            this.react && this.react()
        })
    }

    content(data) {
        const container = this.modal.container('', `@${data.username}`, 'Généalogie', true)
        this.modal.onContentMounted(async () => {
            this.react = (await import("@root/components/genealogy/chart-org"))
                .default(container.contentElement(), data.genealogies, false)
        })
        return container.content
    }

    disconnect() {
        this.modal && this.modal.destroy()
    }

}
