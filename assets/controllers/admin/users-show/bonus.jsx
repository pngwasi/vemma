import React, { useCallback, useEffect, useMemo, useState } from 'react'
import ReactInstance from '@root/utils/react/react-instance.jsx'
import Spinner from '@root/components/spinner'
import { useFetch } from '@root/utils/react/hooks'
import Button from '@root/components/Button'
import { confirmed } from '@root/functions/functions'


const Bonus = ({ id, tree }) => {
    tree = Boolean(parseInt(tree, 10))

    const [availables, setAvailables] = useState([])
    const [saved, setSaved] = useState([])

    const { loading, fetchAPi } = useFetch(true)

    useEffect(() => {
        fetchAPi(`/vemma/admin/users/bonus/${id}`)
            .then(({ data: { saved, availables } }) => {
                setAvailables(availables)
                setSaved(saved)
            })
    }, [id])


    const saveBonus = useCallback((bonus) => {
        if (!confirmed()) return
        fetchAPi(`/vemma/admin/users/bonus/${id}/${bonus}`, 'post')
            .then(({ data: { saved, availables } }) => {
                setAvailables(availables)
                setSaved(saved)
            })
    }, [])

    return <>
        {loading && <Spinner />}
        <div className="row">
            <div className="col-lg-6 my-5">
                <h5>Bonus disponible</h5>
                <div className="table-responsive">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Downlines</th>
                                <th scope="col">Label</th>
                                <th scope="col">Enregistrer</th>
                            </tr>
                        </thead>
                        <tbody>
                            {availables.map((bonus) => {
                                return <tr>
                                    <td>{bonus.downlines}</td>
                                    <td>{bonus.label}</td>
                                    <td>
                                        {tree && <Button text="Enregistrer" onClick={() => saveBonus(bonus.id)} />}
                                        {!tree && <span>Arbre généalogique incomplet</span>}
                                    </td>
                                </tr>
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="col-lg-6 my-5">
                <h5>Bonus Enregistrés</h5>
                <div className="table-responsive">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Downlines</th>
                                <th scope="col">Label</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            {saved.map((bonus) => {
                                return <tr>
                                    <td>{bonus.downlines}</td>
                                    <td>{bonus.label}</td>
                                    <td>{new Date(bonus.created_at).toLocaleString('fr')}</td>
                                </tr>
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default (el,) => ReactInstance(
    <Bonus
        id={el.getAttribute('data-id')}
        tree={el.getAttribute('data-tree')}
    />,
    el)