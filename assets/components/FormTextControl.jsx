import { useErrorInput } from '@root/utils/react/hooks'
import React from 'react'


const FormTextControl = ({ label, type = 'text', name, defaultValue = undefined, readOnly = false, textarea = false, errors = {} }) => {
    const { error, onKeyUp } = useErrorInput(name, errors)

    return <div className="mb-3">
        <label htmlFor={`${name}-1`} className="form-label text-xs text-muted">
            {label}
        </label>
        {
            textarea ?
                <textarea
                    onKeyUp={onKeyUp}
                    required
                    defaultValue={defaultValue}
                    readOnly={readOnly}
                    name={name}
                    className={`form-control ${error ? 'is-invalid' : ''} border-radius-0`} id={`${name}-1`}
                    rows={5}></textarea> :
                <input
                    onKeyUp={onKeyUp}
                    type={type}
                    required
                    defaultValue={defaultValue}
                    readOnly={readOnly}
                    name={name}
                    className={`form-control ${error ? 'is-invalid' : ''} border-radius-0`}
                    id={`${name}-1`} />
        }
        {
            error && (
                <div className="invalid-feedback text-xs">
                    {error}
                </div>
            )
        }
    </div>
}

export default FormTextControl