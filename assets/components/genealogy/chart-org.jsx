import React from 'react'
import ReactInstance from '@root/utils/react/react-instance.jsx'
import { Chart } from "react-google-charts"
import Spinner from '@root/components/spinner'
import './style.scss'


const gnData = (gDatas) => {
    let datas = [];
    for (let index = 1; index <= 13; index++) {
        const g = gDatas[index - 1]

        datas.push([
            {
                v: `${index}`,
                f: /*html*/`<div class="p-1">
                    <b class="text-${g ? 'primary' : 'dark'}">
                        ${g ? '@' + g.username : 'Vide'}
                    </b> 
                    <br> ${index}
                </div>`
            },
            (index == 1 ? '' : `${Math.round(index / 3)}`),
            `${g ? g.fullname : 'Vide'}`
        ])
    }
    return datas
}

const ChartOrg = ({ gDatas, canselect }) => {
    return <div className="table-responsive my-5">
        <Chart
            width={'100%'}
            height="auto"
            chartEvents={[
                {
                    eventName: 'select',
                    callback: ({ chartWrapper }) => {
                        if (!canselect) return

                        const chart = chartWrapper.getChart()
                        const id = chart?.getSelection()[0]?.row
                        if (!gDatas[id]) return

                        const csearch = new URLSearchParams(window.location.search)
                        csearch.set('username', gDatas[id].username)

                        const { origin, pathname } = window.location
                        window.location.replace(origin + pathname + '?' + csearch.toString())
                    },
                },
            ]}
            className="chart-org-table"
            chartType="OrgChart"
            loader={<Spinner />}
            data={[
                ['Name', 'Manager', 'ToolTip'],
                ...gnData(gDatas)
            ]}
            options={{
                allowHtml: true,
                allowCollapse: false,
                size: 'medium'
            }}
            rootProps={{ 'data-testid': '1' }}
        />
    </div>
}

export default (el, gDatas, canselect = true) => ReactInstance(<ChartOrg gDatas={gDatas} canselect={canselect} />, el)