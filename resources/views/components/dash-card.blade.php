@props(['title'])

<div class="card mb-3">
    <div class="card-header bg-light">
        <x-dash-title :title="$title" />
    </div>
    <div class="card-body">
        {{$slot}}
    </div>
</div>