<button {{ $attributes->merge(['class' => 'btn btn-primary mt-3']) }}>
    {{ $slot }}
</button>