<div class="container">
    <div class="row justify-content-center min-vh-100 py-6">
        <div {{ $attributes->merge(['class' => "col-sm-12 col-md-8 col-lg-6 col-xl-5 col-xxl-4"]) }}>
            <x-auth-logo />
            <div class="card">
                <div class="card-body p-4 p-sm-5">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>