<div>
    @can('super-admin')
    {{ $slot }}
    @endcan

    @cannot('super-admin')
    <h5 class="text-warning text-center">{{ __("Vous devez être un super administrateur.") }}</h5>
    @endcannot
</div>