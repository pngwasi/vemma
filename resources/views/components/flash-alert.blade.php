@props(['type', 'session'])


@if (session($session))
<div class="alert alert-{{ $type }} mb-3" role="alert">
    {{ session($session) }}
</div>
@endif