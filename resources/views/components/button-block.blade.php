<button {{ $attributes->merge(['class' => 'btn btn-primary d-block w-100 mt-3']) }} >
    {{ $slot }}
</button>