@props(['title'])

<h5 class="pvr-header text-black-80">
    {{ __($title) }}
</h5>