<a class="d-flex justify-content-center mb-4" href="/">
    <img class="me-2" src="{{ asset('favicon/vemma.png') }}" alt="{{ $app_name }}" width="60">
    <span class="font-sans-serif fw-bolder fs-5 d-inline-block">{{ $app_name }}</span>
</a>