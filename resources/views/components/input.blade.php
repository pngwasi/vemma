<div>
    @if ($optional || $label)
    <label class="form-label">
        @if ($label)
        {{ __($label) }}
        @endif
        @if ($optional)
        <small>({{ __($optionalText) }})</small>
        @endif
    </label>
    @endif

    @if ($hasWire)
    <input {{ $attributes }} placeholder="{{ __($inputLabel) }}" wire:model.lazy="{{ $model }}" name="{{ $model }}"
        class="form-control @error($model) is-invalid @enderror">
    @else
    <input {{ $attributes }} placeholder="{{ __($inputLabel) }}" name="{{ $model }}"
        class="form-control @error($model) is-invalid @enderror">
    @endif


    @error($model)<div class="invalid-feedback">{{$message}}</div>@enderror
</div>