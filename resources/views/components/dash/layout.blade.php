@props(['sidebar', 'navbar'])

<x-app-layout>
    <div class="wrapper-dash wrapper d-flex align-items-stretch" data-controller="layout">
        @include($sidebar)

        <div id="main-content">
            @include($navbar)

            <div class="container-fluid">
                {{ $slot }}
            </div>
        </div>
    </div>

    <div class="close-layer cursor-pointer" id="close-layer"></div>
</x-app-layout>