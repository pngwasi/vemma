<x-admin-layout>
    <x-breadcrumb :items="[
        [
            'name' => 'Paiements',
            'active' => true
        ]
    ]" />

    <div data-controller="admin--payments">
        <x-nav-tab :tabs="['Paiements', 'Nouveau Paie', 'Options', 'Mode de paiement']">
            <x-slot name="paiements">
                <x-livewire.frame ref="tables.payments-table" />
            </x-slot>
            <x-slot name="nouveau_paie">
                <x-super-admin>
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <x-livewire.frame frameId="users-search-table" ref="tables.payments.users-search-table" />
                        </div>
                    </div>
                </x-super-admin>
            </x-slot>
            <x-slot name="options">
                <x-super-admin>
                    <div class="row">
                        <div class="col-lg-6 mb-4">
                            <x-flash-alert type="success" session="updated" />
                            <x-flash-alert type="danger" session="error" />
                            <form method="POST" class="mt-4" autocomplete="off"
                                action="{{ route('admin.payments.options') }}">
                                @csrf
                                @method('PUT')
                                <div class="mb-3">
                                    <x-label>
                                        {{ __("Definier le montant que l'Définir le montant que l'utilisateur devra payer lors de l'activation de son compte.") }}
                                    </x-label>
                                    <x-input model="user_payment" type="text"
                                        value="{{ old('user_payment') ?: ($setting ? $setting->user_payment : '0') }}"
                                        label="Paiement de l'utilisateur" />
                                </div>

                                <div class="mb-3">
                                    <x-label>
                                        {{ __("Définir le montant en pourcentage qui sera utilisé dans le calcul de balance après l'achèvement d'arborescence de l'utilisateur.") }}
                                        @if ($setting)
                                        <br>
                                        Ex:
                                        <b>
                                            <span>
                                                {{ $setting->user_percentage }} x
                                                ({{ $setting->user_payment }} x {{ $childTree }}) / {{ 100 }}
                                            </span>
                                            <span>=</span>
                                            <span>{{ $currency }}{{ $setting->user_percentage * ( $setting->user_payment * $childTree) / 100 }}</span>
                                        </b>
                                        @endif
                                    </x-label>
                                    <x-input model="user_percentage" type="text"
                                        value="{{ old('user_percentage') ?: ($setting ? $setting->user_percentage : '0') }}"
                                        label="Pourcentage d'utilisateurs (%)" />
                                </div>

                                <div class="mb-3">
                                    <x-button type="submit">
                                        {{ __("Enregistrer")}}
                                    </x-button>
                                </div>
                            </form>
                        </div>
                    </div>
                </x-super-admin>
            </x-slot>

            <x-slot name="mode_de_paiement">
                <x-super-admin>
                    <div class="row">
                        <div class="col-lg-6 mb-3">
                            <h6>{{ __("Mode de paiement") }}</h6>
                            <x-flash-alert type="success" session="created" />
                            <form method="POST" class="mt-4" autocomplete="off"
                                action="{{ route('admin.payments.method') }}">
                                @csrf
                                @method('PUT')
                                <div class="mb-3">
                                    <x-input model="method" type="text" value="{{ old('method') ?: '' }}"
                                        label="Nom de la méthode" />
                                </div>
                                <div class="mb-3">
                                    <x-button type="submit">
                                        {{ __("Enregistrer")}}
                                    </x-button>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-6">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">{{ __("Méthode") }}</th>
                                        <th scope="col">{{ __("Supprimer") }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($methods as $method)
                                    <tr>
                                        <td>{{ $method->method }}</td>
                                        <td>
                                            <form method="POST" onsubmit="return confirm('{{ __('Supprimer ?') }}')"
                                                autocomplete="off"
                                                action="{{ route('admin.payments.method.delete', ['id' => $method->id]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger text-sm btn-sm">
                                                    {{ __("Supprimer") }}
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </x-super-admin>
            </x-slot>
        </x-nav-tab>
    </div>
</x-admin-layout>