<x-admin-layout>
    <x-breadcrumb :items="[
        [
            'name' => 'Revenus',
            'active' => true
        ]
    ]" />

    <x-livewire.frame ref="tables.incomes-table" />
</x-admin-layout>