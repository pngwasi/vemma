<x-admin-layout>
    <x-breadcrumb :items="[
        [
            'name' => 'Primes',
            'active' => true
        ]
    ]" />

    <x-nav-tab :tabs="['Bonus', 'Avantages']">

        <x-slot name="bonus">
            <x-flash-alert type="danger" session="error" />
            <x-flash-alert type="success" session="created" />
            <div class="row">
                <div class="col-lg-6">
                    <form method="POST" class="mb-4" autocomplete="off" action="{{ route('admin.primes') }}">
                        @csrf
                        <div class="mb-3">
                            <x-input model="label" type="text" value="{{ old('label') ?: '' }}"
                                label="Label(ex: TV, Smartphone)" />
                        </div>
                        <div class="mb-3">
                            <x-input model="downlines" type="number" value="{{ old('downlines') ?: '' }}"
                                label="Downlines" />
                        </div>

                        <div class="mb-3">
                            <x-button type="submit">
                                {{ __("Enregistrer")}}
                            </x-button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">{{ __("Downlines") }}</th>
                                <th scope="col">{{ __("Label") }}</th>
                                <th scope="col">{{ __("Supprimer") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($primes as $prime)
                            <tr>
                                <td>{{ $prime->downlines }}</td>
                                <td>{{ $prime->label }}</td>
                                <td>
                                    <form method="POST" onsubmit="return confirm('{{ __('Supprimer ?') }}')"
                                        autocomplete="off"
                                        action="{{ route('admin.primes.delete', ['id' => $prime->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger text-sm btn-sm">
                                            {{ __("Supprimer") }}
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </x-slot>


        <x-slot name="avantages">
            <x-flash-alert type="danger" session="error" />
            <x-flash-alert type="success" session="created" />
            <div class="row">
                <div class="col-lg-6">
                    <form method="POST" class="mb-4" autocomplete="off" action="{{ route('admin.advantages') }}">
                        @csrf
                        <div class="mb-3">
                            <x-input model="amount" type="text" value="{{ old('amount') ?: '' }}" label="Montant" />
                        </div>
                        <div class="mb-3">
                            <x-input model="downlines" type="number" value="{{ old('downlines') ?: '' }}"
                                label="Downlines" />
                        </div>

                        <div class="mb-3">
                            <x-button type="submit">
                                {{ __("Enregistrer")}}
                            </x-button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">{{ __("Downlines") }}</th>
                                <th scope="col">{{ __("Montant") }}</th>
                                <th scope="col">{{ __("Supprimer") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($advantages as $advantage)
                            <tr>
                                <td>{{ $advantage->downlines }}</td>
                                <td>{{ $advantage->amount }}</td>
                                <td>
                                    <form method="POST" onsubmit="return confirm('{{ __('Supprimer ?') }}')"
                                        autocomplete="off"
                                        action="{{ route('admin.advantages.delete', ['id' => $advantage->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger text-sm btn-sm">
                                            {{ __("Supprimer") }}
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </x-slot>
    </x-nav-tab>
</x-admin-layout>