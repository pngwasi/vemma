<x-guest-layout>
    <x-auth-card>
        <div class="row justify-content-between mb-2">
            <div class="col-12 text-center">
                <h5>{{ __("Se connecter") }}</h5>
                <p>
                    <b><small>{{ $app_name }} {{ __("Admin") }}</small></b>
                </p>
            </div>
        </div>

        <form method="POST" action="{{ route('admin.login') }}">
            @csrf
            <div class="mb-3">
                <x-input type="email" model="email" value="{{ old('email') ?: '' }}" inputLabel="Adresse e-mail" />
            </div>
            <div class="mb-3">
                <x-input type="password" model="password" inputLabel="Mot de passe" />
            </div>
            <div class="row justify-content-between">
                <div class="col-auto">
                    <div class="form-check mb-0">
                        <input class="form-check-input" type="checkbox" id="basic-checkbox" name="remember_me">
                        <label class="form-check-label" for="basic-checkbox">
                            {{ __("Se souvenir") }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <x-button-block type="submit">
                    {{ __("Connexion")}}
                </x-button-block>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>