<x-admin-layout>
    <x-breadcrumb :items="[
        [
            'name' => 'Retraits',
            'active' => true
        ]
    ]" />

    <x-livewire.frame ref="tables.withdrawals-table" />
</x-admin-layout>