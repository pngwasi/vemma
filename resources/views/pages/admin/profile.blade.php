<x-admin-layout>
    <div class="row">
        <div class="col-lg-5 col-md-8">
            <x-flash-alert type="success" session="updated" />
        </div>
    </div>

    <x-nav-tab :tabs="['Profil', 'Changer le mot de passe']">
        <x-slot name="profil">
            <x-dash-card title="Profil">
                <div class="row">
                    <div class="col-lg-5 col-md-8">
                        <form method="POST" class="mt-4" action="{{ route('admin.profile') }}">
                            @method('PUT')
                            @csrf

                            <div class="mb-3">
                                <x-input model="name" type="text" value="{{ old('name') ?: $auth->name }}"
                                    inputLabel="Nom" />
                            </div>

                            <div class="mb-3">
                                <x-input model="email" type="email" readonly="" value="{{ $auth->email }}"
                                    inputLabel="Adresse e-mail" />
                            </div>

                            <div class="mb-3">
                                <x-button type="submit">
                                    {{ __("Enregistrer")}}
                                </x-button>
                            </div>
                        </form>
                    </div>
                </div>
            </x-dash-card>
        </x-slot>

        <x-slot name="changer_le_mot_de_passe">
            <x-dash-card title="Changer le mot de passe">

                <div class="row">
                    <div class="col-lg-5 col-md-8">
                        <form method="POST" class="mt-4" action="{{ route('admin.profile.password') }}">
                            @method('PUT')
                            @csrf

                            <div class="mb-3">
                                <x-input model="password" type="password" inputLabel="Mot de passe actuel" />
                            </div>

                            <div class="mb-3">
                                <x-input model="password" type="password" inputLabel="Nouveau mot de passe" />
                            </div>

                            <div class="mb-3">
                                <x-input model="new_password_confirmation" type="password"
                                    inputLabel="Confirmez le mot de passe" />
                            </div>

                            <div class="mb-3">
                                <x-button type="submit">
                                    {{ __("Sauvegarder les modifications")}}
                                </x-button>
                            </div>
                        </form>
                    </div>
                </div>
            </x-dash-card>
        </x-slot>
    </x-nav-tab>
</x-admin-layout>