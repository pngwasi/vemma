<div class="row">
    <div class="col-lg-6">
        <x-dash-card title="Paiements">
            <canvas data-admin--home-target="payments-chart"></canvas>
        </x-dash-card>
    </div>
    <div class="col-lg-6">
        <x-dash-card title="Revenus">
            <canvas data-admin--home-target="incomes-chart"></canvas>
        </x-dash-card>
    </div>
</div>

<script type="text/javascript">
    window.chart_incomes = @json($chart_incomes);
    window.chart_payments = @json($chart_payments);
</script>