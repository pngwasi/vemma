<div class="row g-3">
    <div class="col-sm-6 col-md-4">
        <div class="card overflow-hidden" style="min-width: 12rem">
            <div class="card-body position-relative">
                <h6>{{ __("Revenus") }}</h6>
                <div class="display-4 fs-4 mb-2 fw-normal font-sans-serif text-warning">
                    {{ $currency }}{{ $incomes }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="card overflow-hidden" style="min-width: 12rem">
            <div class="card-body position-relative">
                <h6>{{ __("Utilisateurs") }}</h6>
                <div class="display-4 fs-4 mb-2 fw-normal font-sans-serif text-info">
                    {{ $users }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="card mb-3 overflow-hidden" style="min-width: 12rem">
            <div class="card-body position-relative">
                <h6>{{ __("Paiements Total") }}</h6>
                <div class="display-4 fs-4 mb-2 fw-normal font-sans-serif text-success">
                    {{ $currency }}{{ $payments }}
                </div>
            </div>
        </div>
    </div>
</div>