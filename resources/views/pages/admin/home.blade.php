<x-admin-layout>
    <div data-controller="admin--home">
        @include('pages.admin.home.cards')
        @include('pages.admin.home.charts')
    </div>
</x-admin-layout>