<x-admin-layout>
    <div data-controller="admin--settings">
        <x-nav-tab :tabs="['Nouveau administrateur', 'Administrateurs']">

            <x-slot name="nouveau_administrateur">
                <x-dash-card title="Nouveau administrateur">
                    <x-flash-alert type="danger" session="error" />
                    <x-flash-alert type="success" session="created" />

                    <div class="row">
                        <div class="col-lg-5 col-md-8">
                            <form method="POST" class="mt-4" action="{{ route('admin.settings.admins') }}">
                                @csrf

                                <div class="mb-3">
                                    <x-input model="name" type="text" value="{{ old('name') ?: '' }}"
                                        inputLabel="Nom" />
                                </div>

                                <div class="mb-3">
                                    <x-input model="email" type="email" value="{{ old('email') ?: '' }}"
                                        inputLabel="Adresse e-mail" />
                                </div>

                                <div class="mb-3">
                                    <x-input model="password" type="password" inputLabel="Mot de passe" />
                                </div>

                                <div class="mb-3">
                                    <x-input model="password_confirmation" type="password"
                                        inputLabel="Confirmez le mot de passe" />
                                </div>

                                <div class="mb-3">
                                    <x-button type="submit">
                                        {{ __("Enregistrer")}}
                                    </x-button>
                                </div>
                            </form>
                        </div>
                    </div>
                </x-dash-card>
            </x-slot>

            <x-slot name="administrateurs">
                <x-super-admin>
                    <x-livewire.frame ref="tables.admins-table" />
                </x-super-admin>
            </x-slot>

        </x-nav-tab>
    </div>
</x-admin-layout>