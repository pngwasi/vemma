<x-admin-layout>
    <x-breadcrumb :items="[
        [
            'name' => 'Utilisateurs',
            'active' => false,
            'link' => route('admin.users')
        ],
        [
            'name' => 'Profil - @'.$user->username,
            'active' => true
        ],
    ]" />
    <div data-controller="admin--users-show">
        <x-nav-tab :tabs="['Profil', 'Paiement', 'Retraits', 'Généalogie', 'Mode de paiement', 'Bonus']">
            <x-slot name="profil">
                @include('pages.dashboard.home.cards', [
                'available_balance' => $user->availableBalance(),
                'encashment' => $user->amountEncashment(),
                'balance' => $user->amountBalance(),
                ])
                <x-dash-card title="Détails">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><b>{{ __("Downlines Direct") }}:</b></td>
                                            <td>{{ $user->downlines()->count() }}/{{ $childTree }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Prénom") }}:</b></td>
                                            <td>{{ $user->profile->firstname }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Nom de famille") }}:</b></td>
                                            <td>{{ $user->profile->lastname }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Autre Nom") }}:</b></td>
                                            <td>{{ $user->profile->middlename }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Sexe") }}:</b></td>
                                            <td>{{ $user->profile->gender }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Date de naissance") }}:</b></td>
                                            <td>{{ $user->profile->birthdate }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Numéro d'identification fiscale") }}:</b></td>
                                            <td>{{ $user->profile->tax_identification_number }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($user->upline)
                                        <tr>
                                            <td><b>{{ __("Upline") }}:</b></td>
                                            <td>{{ "@".$user->userUpline->username }}</td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td><b>{{ __("Downlines Total") }}:</b></td>
                                            <td>{{ $user->downlines_total }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Nom d'utilisateur") }}:</b></td>
                                            <td>{{ "@".$user->username }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Adresse e-mail") }}:</b></td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Numéro de téléphone") }}:</b></td>
                                            <td>{{ $user->profile->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Adresse") }}:</b></td>
                                            <td>{{ $user->profile->address }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Pays") }}:</b></td>
                                            <td>{{ $user->profile->country }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{ __("Région/État") }}:</b></td>
                                            <td>{{ $user->profile->region }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>{{ __("Description") }}:</b></td>
                                    <td>
                                        @if ($user->profile->description)
                                        <div class="border border-darken-1 p-2">
                                            {{ $user->profile->description }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>{{ __("Statut") }}:</b></td>
                                    <td>
                                        @if ($user->trashed())
                                        <span class="badge bg-danger">{{ __("Inactif") }}</span>
                                        @else
                                        <span class="badge bg-success">{{ __("Actif") }}</span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </x-dash-card>
            </x-slot>

            <x-slot name="paiement">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="card mb-3">
                            <div class="card-header bg-light">
                                {{ __("Montant payé") }}:
                                {{ $currency }}{{ $user->payment ? $user->payment->amount : 0 }}
                            </div>
                            <div class="card-body text-center">
                                @if ($user->payment)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ __("Montant") }}:</td>
                                                <td>{{ $currency }}{{ $user->payment->amount }}</td>
                                            </tr>
                                            <tr>
                                                <td>{{ __("Upline") }}:</td>
                                                <td>{{ '@'.($user->userUpline ? $user->userUpline->username : '') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @else
                                {{ __("Aucun paiement n'a été effectué.") }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </x-slot>

            <x-slot name="retraits">
                <x-livewire.frame ref="tables.user-withdraws" user="{{ $user->id }}" />
            </x-slot>

            <x-slot name="genealogie">
                <script type="text/javascript">
                    window.genealogiesChart = @json($user->genealogy());
                </script>
                <div data-admin--users-show-target="chart-org">
                    <div class="my-5">
                        <x-spinner />
                    </div>
                </div>
            </x-slot>

            <x-slot name="mode_de_paiement">
                <div class="row">
                    <div class="col-lg-6">
                        <x-flash-alert type="success" session="created" />
                        <form method="POST" class="mt-4" autocomplete="off"
                            action="{{ route('admin.users.show.method', ['username' => $user->username]) }}">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <x-input model="method" type="text" value="{{ old('method') ?: '' }}"
                                    label="Nom de la méthode" />
                            </div>
                            <div class="mb-3">
                                <x-input model="account_number" type="text" value="{{ old('method') ?: '' }}"
                                    label="Numéro de compte" />
                            </div>
                            <div class="mb-3">
                                <x-button type="submit">
                                    {{ __("Enregistrer")}}
                                </x-button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-6">
                        @foreach ($methods as $method)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">{{ __("Méthode") }}</th>
                                    <th scope="col">{{ __("Numéro de compte") }}</th>
                                    <th scope="col">{{ __("Supprimer") }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($methods as $method)
                                <tr>
                                    <td>{{ $method->method }}</td>
                                    <td>{{ $method->account_number }}</td>
                                    <td>
                                        <form method="POST" onsubmit="return confirm('{{ __('Supprimer ?') }}')"
                                            autocomplete="off" action="{{ route('admin.users.show.method.delete', [
                                                'username' => $user->username,
                                                'id' => $method->id
                                                ]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-link text-sm btn-sm">
                                                {{ __("Supprimer") }}
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endforeach
                    </div>
                </div>
            </x-slot>
            <x-slot name="bonus">
                <div data-admin--users-show-target="bonus" data-id="{{ $user->id }}" data-tree="{{ $user->hasCompletedTree() }}">
                    <x-spinner />
                </div>
            </x-slot>
        </x-nav-tab>
    </div>
</x-admin-layout>