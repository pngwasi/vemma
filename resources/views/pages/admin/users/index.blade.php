<x-admin-layout>
    <x-breadcrumb :items="[
        [
            'name' => 'Utilisateurs',
            'active' => true
        ]
    ]" />
    <div data-controller="admin--users">
        <x-livewire.frame ref="tables.users-table" />
    </div>
</x-admin-layout>