<x-dashboard-layout>
    @include('pages.dashboard.home.cards')

    <div class="row">
        <div class="col-lg-8">
            @if (!$auth->active)
                @include('pages.dashboard.home.activation-payment')
            @endif
            @include('pages.dashboard.home.notifications')
        </div>
        <div class="col-lg-4">
            @include('pages.dashboard.home.account')
        </div>
    </div>
</x-dashboard-layout>