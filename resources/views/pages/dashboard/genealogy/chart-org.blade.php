<script type="text/javascript">
    window.genealogiesChart = @json($genealogiesChart);
</script>
<div data-dashboard--genealogy-target="chart-org">
    <div class="my-5">
        <x-spinner />
    </div>
</div>