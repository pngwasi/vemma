<x-dash-card title="Généalogie">
    <div class="d-flex">
        <form class="d-flex" method="GET" action="">
            <x-input class="form-control me-2" type="username" model="username" value="{{ request('username') }}"
                inputLabel="Entrez le nom d'utilisateur ici" aria-label="username" />
            <button class="btn btn-outline-primary mx-3" type="submit">{{ __("Recherche") }}</button>
        </form>
        <form method="GET" action="{{ request()->url() }}">
            <button class="btn btn-primary" type="submit">{{ __("Acceuil") }}</button>
        </form>
    </div>
</x-dash-card>