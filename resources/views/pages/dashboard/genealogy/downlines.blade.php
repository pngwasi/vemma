<x-dash-card title="Downlines">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{ __("Nom d'utilisateur") }}</th>
                    <th scope="col">{{ __("Nom") }}</th>
                    <th scope="col">{{ __("Upline") }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($genealogies as $key => $item)
                @if ($item !== null)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ "@".$item->username }}</td>
                    <td>{{ $item->fullname  }}</td>
                    <td>{{ $item->upline ? $item->upline->userUpline->username : '--' }}</td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</x-dash-card>