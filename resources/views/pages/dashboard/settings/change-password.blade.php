<x-dashboard-layout>
    <x-dash-card title="Changer le mot de passe">
        <x-flash-alert type="success" session="updated" />

        <div class="row">
            <div class="col-lg-5 col-md-8">
                <form method="POST" class="mt-4" action="{{ route('dashboard.settings.change-password') }}">
                    @method('PUT')
                    @csrf

                    <div class="mb-3">
                        <x-input model="password" type="password" inputLabel="Mot de passe actuel" />
                    </div>

                    <div class="mb-3">
                        <x-input model="password" type="password" inputLabel="Nouveau mot de passe" />
                    </div>

                    <div class="mb-3">
                        <x-input model="new_password_confirmation" type="password"
                            inputLabel="Confirmez le mot de passe" />
                    </div>

                    <div class="mb-3">
                        <x-button type="submit">
                            {{ __("Sauvegarder les modifications")}}
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </x-dash-card>
</x-dashboard-layout>