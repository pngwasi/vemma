<x-dashboard-layout>
    <div data-controller="dashboard--profile">
        <x-dash-card title="Paramètres de profil">

            <x-flash-alert type="success" session="updated" />

            <form action="{{ route('dashboard.settings.profile') }}" method="post">
                @csrf
                @method("PUT")
                <div class="row">
                    <div class="col-lg-6">
                        <x-dash-card title="Renseignements personnels">

                            <div class="row">
                                <div class="mb-3 col-lg-6">
                                    <x-input model="firstname" type="text"
                                        value="{{ old('firstname') ?: $profile->firstname }}" label="Prénom" />
                                </div>
                                <div class="mb-3 col-lg-6">
                                    <x-input model="lastname" type="text"
                                        value="{{ old('lastname') ?: $profile->lastname }}" label="Nom de famille" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3 col-lg-6">
                                    <x-input model="middlename" type="text"
                                        value="{{ old('middlename') ?: $profile->middlename }}" label="Autre Nom" />
                                </div>

                                <div class="mb-3 col-lg-6">
                                    <div>
                                        <label class="form-label">
                                            Sexe
                                        </label>
                                        <select class="form-select" name="gender" aria-label="Default select example">
                                            <option value="">{{ __("Sexe") }}</option>
                                            @foreach ([['name' => 'Homme', 'value' =>'male'], ['name' => 'Femme',
                                            'value' =>
                                            'female']] as $item)
                                            <option value="{{ $item['value'] }}"
                                                {{ $profile->gender == $item['value'] ? 'selected' : '' }}>
                                                {{ __($item['name']) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <x-input model="birthdate" type="date"
                                    value="{{ old('birthdate') ?: $profile->birthdate }}" label="Date de naissance" />
                            </div>

                            <div class="mb-3">
                                <x-input model="tax_identification_number" type="text"
                                    value="{{ old('tax_identification_number') ?: $profile->tax_identification_number }}"
                                    label="Numéro d'identification fiscale" />
                            </div>

                            <div class="mb-3">
                                <x-input model="address" type="text" value="{{ old('address') ?: $profile->address }}"
                                    label="Adresse" />
                            </div>

                            <div class="mb-3">
                                <x-input model="country" type="text" value="{{ old('country') ?: $profile->country }}"
                                    label="Pays" />
                            </div>

                            <div class="mb-3">
                                <x-input model="region" type="text" value="{{ old('region') ?: $profile->region }}"
                                    label="Région/État" />
                            </div>

                        </x-dash-card>
                    </div>
                    <div class="col-lg-6">
                        <x-dash-card title="Autres informations">
                            <div class="mb-3">
                                <x-input model="phone" type="tel" value="{{ old('phone') ?: $profile->phone }}"
                                    label="Numéro de téléphone" />
                            </div>

                            <div class="mb-3">
                                <x-input type="email" model="email" readonly=""
                                    value="{{ old('email') ?: $auth->email }}" label="Adresse e-mail" />
                            </div>

                            <div class="mb-3">
                                <label for="description" class="form-label">{{ __('À propos de moi') }}</label>
                                <textarea class="form-control @error('description') is-invalid @enderror"
                                    name="description" id="description" rows="3">
                                {{ old('description') ?: $profile->description }}
                                </textarea>
                                @error('description')<div class="invalid-feedback">{{$message}}</div>@enderror
                            </div>
                        </x-dash-card>
                    </div>
                </div>

                <div class="mb-3">
                    <x-button type="submit">
                        {{ __("Sauvegarder les modifications")}}
                    </x-button>
                </div>
            </form>
        </x-dash-card>

        <x-dash-card title="Image profil">
            <form action="{{ route('dashboard.settings.profile.image') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="upload--img-u">
                    <label>{{ __('Image') }} *</label>
                    <div class="upload-image">
                        <div>
                            <img {{ Auth::user()->image  ? "src=".Auth::user()->image : '' }}
                                data-dashboard--profile-target="imageDisplay" style="width: 100%; height: 100%;" />
                        </div>
                        <input type="file" data-action="dashboard--profile#onChangeImage" id="file" accept="image/*"
                            class="is-invalid file-upload-m" name="image">
                    </div>
                </div>
                @error('image')<div class="text-danger">{{$message}}</div>@enderror

                <x-button type="submit">
                    {{ __("Sauvegarder")}}
                </x-button>
            </form>
        </x-dash-card>
    </div>

</x-dashboard-layout>