<x-dashboard-layout>
    <x-dash-card title="Encaissement">
        <div class="row">
            <div class="col-md-5 col-lg-3 mb-3">
                <div class="card h-md-100 shadow-none border">
                    <div class="card-body">
                        <h6 class="mb-0 mt-2 d-flex align-items-center card-title">
                            <b>{{ __("Solde disponible") }}</b>
                        </h6>
                        <div class="row flex-grow-1">
                            <div class="col">
                                <div class="fs-4 font-weight-normal text-sans-serif text-700 line-height-1 mb-1">
                                    {{ $currency.$balance }}
                                </div>
                                <span class="badge badge-pill fs--2 badge-soft-success">
                                    {{ _('Total encaissement') }} {{ $currency.$encashmentsTotal }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-lg-9">
                <form action="{{ route('dashboard.encashment') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <x-input type="number" model="amount" label="Montant" />
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <label for="methods" class="form-label">Méthode</label>
                            <select name="method" class="form-select @error('method') is-invalid @enderror" id="methods"
                                aria-describedby="methodsFeedback" required>
                                <option selected disabled value="">{{ __("Choisir") }}...</option>
                                @foreach ($methods as $method)
                                <option value="{{ $method->method }}">{{ $method->method }}</option>
                                @endforeach
                            </select>
                            @error('method')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                    </div>
                    <x-button>
                        {{ __("Encaisser") }}
                    </x-button>
                </form>
            </div>
        </div>

    </x-dash-card>

    <x-dash-card title="Historique d'encaissement">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">{{ __("Référence") }}#</th>
                        <th scope="col">{{ __("Date") }}</th>
                        <th scope="col">{{ __("Nom") }}</th>
                        <th scope="col">{{ __("Méthode") }}</th>
                        <th scope="col">{{ __("Numéro de compte") }}</th>
                        <th scope="col">{{ __("Brute") }}</th>
                        <th scope="col">{{ __("Net") }}</th>
                        <th scope="col">{{ __("Devise") }}</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach ($genealogies as $key => $item)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ "@".$item->username }}</td>
                    <td>{{ $item->fullname  }}</td>
                    <td>{{ $item->upline ? $item->upline->userUpline->username : '--' }}</td>
                    </tr>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
    </x-dash-card>
</x-dashboard-layout>