<x-dash-card title="Compte - {{ '@'.$auth->username }}">
    <div class="list-group">
        <a href="javascript:;" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __("Statut") }}</h5>
            </div>
            <p class="mb-1 text-{{ $auth->active ?'success':'warning' }}">
                {{ __($auth->active ? 'Actif' : 'Activation requise') }}
            </p>
        </a>
        <a href="javascript:;" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __("Pays") }}</h5>
            </div>
            <p class="mb-1">{{ $profile->country }}</p>
        </a>
        <a href="javascript:;" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __("Région/État") }}</h5>
            </div>
            <p class="mb-1">{{ $profile->region }}</p>
        </a>
        <a href="javascript:;" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __("Downlines") }}</h5>
            </div>
            <p class="mb-1">{{ __("Direct") }}: {{ $auth->downlines()->count() }}/{{ $childTree }}</p>
            <p class="mb-1">{{ __("Total") }}: {{ $auth->downlines_total }}</p>
        </a>
        <a href="javascript:;" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __("Email") }}</h5>
            </div>
            <p class="mb-1">{{ $auth->email }}</p>
        </a>
        <a href="javascript:;" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __("Numéro d'identification fiscale") }}</h5>
            </div>
            <p class="mb-1">{{ $profile->tax_identification_numbe }}</p>
        </a>
    </div>
</x-dash-card>