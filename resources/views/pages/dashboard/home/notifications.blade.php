<x-dash-card title="Bienvenu! {{ $auth->fullname }}">
    <h5 class="card-title">
        <button type="button" class="btn btn-light">
            {{ __("Notifications") }} <span class="badge bg-primary">{{ $auth->unreadNotifications()->count() }}</span>
        </button>
    </h5>
    <div class="card-text">
        @forelse ($auth->notifications()->latest()->get() as $notification)
        <div class="list-group">
            @if ($notification->read())
            <a href="javascript:;" class="list-group-item list-group-item-action list-group-item-light my-2">
                • {{ $notification->data['message'] }}
            </a>
            @else
            <a href="javascript:;" class="list-group-item list-group-item-action my-2 text-primary">
                • {{ $notification->data['message'] }}
            </a>
            @endif
        </div>
        @empty
        <div class="text-center py-5">
            <span class="text-primary">{{ __("Aucune notification trouvée pour l'instant") }}</span>
        </div>
        @endforelse
        <div class="d-none">{{ $auth->unreadNotifications()->update(['read_at' => now()]) }}</div>
    </div>
</x-dash-card>