<x-dash-card title="Activation de compte">
    <p class="card-text text-black-50 text-xs">
        <b>{{ __("Pour l'activation du compte, vous devez payer le montant demandé ci-dessous") }}</b>
    </p>
    <h5>{{ $currency.($setting ? $setting->user_payment : '0') }}</h5>
    <form action="" onsubmit="event.preventDefault()" method="post">
        <div class="row mt-5">
            <div class="col-12 col-sm-12 col-md-9">
                <div style="font-size: 12px" class="mb-2">
                    {{ __("En fait, nous ne prenons en charge que les numéros commençant par") }} <span>(+243)</span>.
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-4 col-5">
                        <div class="form-group m-0">
                            <input type="text" placeholder="{{ __("Airtel") }}" readOnly class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-8 col-7">
                        <div class="form-group m-0">
                            <input type="text" placeholder="{{ __("Numéro de téléphone") }}" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="form-group form-check mt-3">
                    <input type="checkbox" class="form-check-input" id="defaultNumber" />
                    <label class="form-check-label" for="defaultNumber">
                        {{ __("Utilisez ce numéro par défaut pour") }} {{ __("Airtel") }}
                    </label>
                </div>

                <button type="submit" class="btn btn-outline-dark btn-sm mt-3">
                    {{ __("Payer") }}
                </button>
            </div>
        </div>
    </form>
</x-dash-card>