<x-dashboard-layout>
    <div data-controller="dashboard--genealogy">
        @include('pages.dashboard.genealogy.chart-org')
        @include('pages.dashboard.genealogy.form')
        @include('pages.dashboard.genealogy.downlines')
    </div>
</x-dashboard-layout>