<x-guest-layout>

    <x-auth-card class="col-sm-12 col-md-10 col-lg-8 col-xl-6 col-xxl-5">
        <div class="row flex-between-center">
            <div class="col-auto">
                <h5>{{ __("S'inscrire") }}</h5>
            </div>
            <div class="col-auto fs--1 text-600">
                <span class="mb-0">{{ __("Vous avez déjà un compte?") }}</span>
                <span>
                    <a href="{{ route('login') }}">{{ __("Se connecter") }}</a>
                </span>
            </div>
        </div>
        <div class="mb-3 fs--1 text-600">
            {{ __("Vous êtes invité par: ") }} <b>{{ $upline->fullname }} - {{ "@".$upline->username  }}</b>
        </div>

        <form method="POST" action="{{ route('register', ['upline' => request('upline')]) }}">
            @csrf

            <div class="row">
                <div class="mb-3 col-12 col-md-6">
                    <x-input model="firstname" type="text" value="{{ old('firstname') ?: '' }}" inputLabel="Prénom" />
                </div>
                <div class="mb-3 col-12 col-md-6">
                    <x-input model="lastname" type="text" value="{{ old('lastname') ?: '' }}"
                        inputLabel="Nom de famille" />
                </div>
            </div>

            <div class="mb-3">
                <x-input model="email" type="email" value="{{ old('email') ?: '' }}" inputLabel="Adresse e-mail" />
            </div>
            <div class="row gx-3">
                <div class="mb-3 col-sm-6">
                    <x-input model="password" type="password" inputLabel="Mot de passe" />
                </div>
                <div class="mb-3 col-sm-6">
                    <x-input model="password_confirmation" type="password" inputLabel="Confirmez le mot de passe" />
                </div>
            </div>

            <div class="form-check">
                <input class="form-check-input @error('accepted') is-invalid @enderror" name="accepted" type="checkbox"
                    id="accepted-privacy" />
                <label class="form-label" for="accepted-privacy">
                    J'accepte les
                    <a href="#!">conditions </a>
                    et
                    <a href="#!"> la politique de confidentialité</a>
                </label>
                @error('accepted')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <div class="mb-3">
                <x-button-block type="submit">
                    {{ __("Nouveau compte")}}
                </x-button-block>
            </div>
        </form>
    </x-auth-card>

</x-guest-layout>