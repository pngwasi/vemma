<x-guest-layout>
    <x-auth-card>
        <div class="text-center">
            <h5 class="mb-0">{{ __("Mot de passe oublié?") }}</h5>
            <small>{{ __("Entrez votre e-mail et nous vous enverrons un lien de réinitialisation.") }}</small>
        </div>


        <form method="POST" class="mt-4" action="{{ route('password.email') }}">
            @csrf
            <x-input type="email" model="email" inputLabel="Adresse e-mail" />
            <div class="mb-3">
                <x-button-block type="submit">
                    {{ __("Envoyer le lien de réinitialisation")}}
                </x-button-block>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>