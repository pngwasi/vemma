<x-guest-layout>
    <x-auth-card>
        <div class="text-center">
            <h5 class="mb-0">{{ __("Réinitialiser le mot de passe") }}</h5>
        </div>

        <form method="POST" class="mt-4" action="{{ route('password.update') }}">
            @csrf
            <!-- Password Reset Token -->
            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <!-- Email Address -->
            <div class="mb-3">
                <x-input type="email" model="email" inputLabel="Adresse e-mail" />
            </div>

            <div class="mb-3">
                <x-input model="password" type="password" inputLabel="Mot de passe" />
            </div>

            <div class="mb-3">
                <x-input model="password_confirmation" type="password" inputLabel="Confirmez le mot de passe" />
            </div>

            <div class="mb-3">
                <x-button-block type="submit">
                    {{ __("Réinitialiser le mot de passe")}}
                </x-button-block>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>