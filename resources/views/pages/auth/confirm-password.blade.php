<x-guest-layout>
    <x-auth-card>
        <div class="text-center">
            <h5 class="mb-0">{{ __("Confirmez le mot de passe") }}</h5>
            <small>
                {{ __("Il s'agit d'une zone sécurisée de l'application. Veuillez confirmer votre mot de passe avant de continuer.") }}
            </small>
        </div>

        <form method="POST" class="mt-4" action="{{ route('password.confirm') }}">
            @csrf
            <x-input type="password" model="password" inputLabel="Mot de passe" />
            <div class="mb-3">
                <x-button-block type="submit">
                    {{ __("Confirmer")}}
                </x-button-block>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>