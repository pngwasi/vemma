<x-guest-layout>
    <x-auth-card>
        <div class="text-center mb-4">
            <h5 class="mb-0">{{ __("Merci de consulter vos emails!") }}</h5>
            <small>
                {{ __("Merci pour votre inscription! Avant de commencer, pouvez-vous vérifier votre adresse e-mail en cliquant sur le lien que nous venons de vous envoyer par e-mail? Si vous n'avez pas reçu l'e-mail, nous vous en enverrons un autre avec plaisir.") }}
            </small>
        </div>


        @if (session('status') == 'verification-link-sent')
        <div class="mb-4 text-success">
            <small>
                {{ __("Un nouveau lien de vérification a été envoyé à l'adresse e-mail que vous avez fournie lors de l'inscription.") }}
            </small>
        </div>
        @endif

        <div class="mt-4">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf
                <x-button-block type="submit">
                    {{ __("Renvoyer l'e-mail de vérification")}}
                </x-button-block>
            </form>

            <form method="POST" class="d-block text-center mt-3" action="{{ route('logout') }}">
                @csrf
                <button type="submit" class="text-dark btn text-sm btn-link">
                    {{ __('Se déconnecter') }}
                </button>
            </form>

            <p class="text-center text-xs"><a href="{{ route("dashboard.home") }}">{{ __("Acceuil") }}</a></p>
        </div>
    </x-auth-card>
</x-guest-layout>