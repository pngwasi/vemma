<button class="button {{ $user->auth ? 'danger': '' }}" wire:click="delete({{ $user->id }})">
    {{ __(!$user->auth ? 'Restaurer' : 'Supprimer') }}
</button>