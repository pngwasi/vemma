<nav class="navbar navbar-expand navbar-light bg-light mb-3">
    <div class="container-fluid">
        <div>
            <button class="custom-menu btn btn-sm d-lg-none" type="button" data-layout-target="collapse-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-menu">
                    <line x1="3" y1="12" x2="21" y2="12"></line>
                    <line x1="3" y1="6" x2="21" y2="6"></line>
                    <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg>
            </button>
            <div class="mx-lg-0 d-inline-block"></div>
            <a class="navbar-brand text-sm text-decoration-none mr-3" id="page_header_title" href="javascript:void(0)">
                Admin: <b>{{ Auth::user()->name }}</b>
            </a>
        </div>
        <div>
            <div class="navbar-collapse" id="navbarNav">
                <ul class="navbar-nav" style="padding-right: 15px">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle profile-dropdown admin text-decoration-none"
                            id="navbarDropdownUser" href="#" role="button" data-bs-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true">
                            {{ Auth::user()->email }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser"
                            data-bs-popper="none">
                            <div class="bg-white rounded-2 py-2">
                                <a class="dropdown-item" href="{{ route('admin.profile') }}">
                                    {{ __("Mon Profil") }}
                                </a>
                                <a class="dropdown-item" href="{{ route('admin.settings') }}">
                                    {{ __("Paramètre") }}
                                </a>
                                <div class="dropdown-divider"></div>
                                <form method="POST" action="{{ route('admin.logout') }}">
                                    @csrf
                                    <button class="dropdown-item btn btn-link" type="submit">
                                        {{ __("Se Déconnecter") }}
                                    </button>
                                </form>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>