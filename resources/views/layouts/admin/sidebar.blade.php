<nav id="sidebar" class="active">
    <div class="p-4">
        <h1>
            <a href="{{ route('admin.home') }}" class="logo d-inline-block text-center">
                {{ $app_name }} <small class="text-xs d-block text-white-50">Admin</small>
            </a>
        </h1>
        <ul class="list-unstyled components mb-5">
            <li class="{{ request()->url() == route('admin.home') ? 'active' : '' }}">
                <a href="{{ route('admin.home') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-home">
                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                    </svg>
                    <span>{{ __("Tableau de bord") }}</span>
                </a>
            </li>
            <li class="{{ Str::contains(request()->url(), route('admin.users')) ? 'active' : '' }}">
                <a href="{{ route('admin.users') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-users">
                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                        <circle cx="9" cy="7" r="4"></circle>
                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                    </svg>
                    <span>{{ __("Utilisateurs") }}</span>
                </a>
            </li>
            <li class="{{ Str::contains(request()->url(), route('admin.payments')) ? 'active' : '' }}">
                <a href="{{ route('admin.payments') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-dollar-sign">
                        <line x1="12" y1="1" x2="12" y2="23"></line>
                        <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
                    </svg>
                    <span>{{ __("Paiements") }}</span>
                </a>
            </li>
            <li class="{{ Str::contains(request()->url(), route('admin.withdrawals')) ? 'active' : '' }}">
                <a href="{{ route('admin.withdrawals') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-credit-card">
                        <rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect>
                        <line x1="1" y1="10" x2="23" y2="10"></line>
                    </svg>
                    <span>{{ __("Retraits") }}</span>
                </a>
            </li>

            <li class="{{ Str::contains(request()->url(), route('admin.incomes')) ? 'active' : '' }}">
                <a href="{{ route('admin.incomes') }}">

                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-trending-up">
                        <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                        <polyline points="17 6 23 6 23 12"></polyline>
                    </svg>
                    <span>{{ __("Revenus") }}</span>
                </a>
            </li>

            <li class="{{ Str::contains(request()->url(), route('admin.primes')) ? 'active' : '' }}">
                <a href="{{ route('admin.primes') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-gift">
                        <polyline points="20 12 20 22 4 22 4 12"></polyline>
                        <rect x="2" y="7" width="20" height="5"></rect>
                        <line x1="12" y1="22" x2="12" y2="7"></line>
                        <path d="M12 7H7.5a2.5 2.5 0 0 1 0-5C11 2 12 7 12 7z"></path>
                        <path d="M12 7h4.5a2.5 2.5 0 0 0 0-5C13 2 12 7 12 7z"></path>
                    </svg>
                    <span>{{ __("Primes") }}</span>
                </a>
            </li>
        </ul>
    </div>
</nav>