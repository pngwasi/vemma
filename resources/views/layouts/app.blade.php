<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="site-name" content="{{ config('app.name') }}" />
    <meta name="lang" content="{{ app()->getLocale() }}" />

    <title>{{ config('app.name', 'Vemma') }}</title>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
    <meta name="msapplication-TileColor" content="#f4f4f4">
    <meta name="theme-color" content="#525f7f">


    {{-- section:assets --}}
    @include('assets.assets', [
        'entries' => 'DEV_SERVER_ENTRIES',
        'assets' => [
            ['tag' => 'link', 'src' => 'app.css'],
            ['tag' => 'script', 'src' => 'manifest.js'],
            ['tag' => 'script', 'src' => 'vendor.js'],
            ['tag' => 'script', 'src' => 'app.js'],
        ]
    ])
</head>

<body @yield('body-attr') class="@yield('body-class')">
    {{ $slot }}
</body>

</html>