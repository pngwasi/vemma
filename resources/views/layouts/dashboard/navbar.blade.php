<nav class="navbar navbar-expand navbar-light bg-light mb-3">
    <div class="container-fluid">
        <div>
            <button class="custom-menu btn btn-sm d-lg-none" type="button" data-layout-target="collapse-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-menu">
                    <line x1="3" y1="12" x2="21" y2="12"></line>
                    <line x1="3" y1="6" x2="21" y2="6"></line>
                    <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg>
            </button>
            <div class="mx-lg-0 d-inline-block"></div>
            <a class="navbar-brand text-sm text-decoration-none mr-3" id="page_header_title" href="javascript:void(0)">
                {{ __("Utilisateur") }} : {{ "@".Auth::user()->username }}
            </a>
        </div>
        <div>
            <div class="navbar-collapse" id="navbarNav">
                <ul class="navbar-nav" style="padding-right: 15px">
                    <li class="nav-item" data-layout-target="open-share" data-username="{{ Auth::user()->username }}"
                        data-url="{{ route('register', ['upline' => Auth::user()->username]) }}">
                        <a class="nav-link notification-indicator notification-indicator-primary px-2 icon-indicator"
                            id="navbarDropdownNotification" href="#" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" data-fa-transform="shrink-6"
                                style="font-size: 30px;transform-origin: 0.4375em 0.5em;"
                                class="svg-inline--fa fa-bell fa-w-14" viewBox="0 0 24 24" fill="none"
                                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                class="feather feather-share-2">
                                <circle cx="18" cy="5" r="3"></circle>
                                <circle cx="6" cy="12" r="3"></circle>
                                <circle cx="18" cy="19" r="3"></circle>
                                <line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line>
                                <line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line>
                            </svg>
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle profile-dropdown text-decoration-none"
                            id="navbarDropdownUser" href="#" role="button" data-bs-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true">
                            <div class="avatar avatar-xl">
                                <img {{ Auth::user()->image  ? "src=".Auth::user()->image : '' }}
                                    class="rounded-circle img-fluid img-thumbnail">
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser"
                            data-bs-popper="none">
                            <div class="bg-white rounded-2 py-2">
                                <a class="dropdown-item" href="{{ route('dashboard.settings.profile') }}">
                                    {{ __("Mon Profil") }}
                                </a>
                                <a class="dropdown-item" href="{{ route('dashboard.settings.change-password') }}">
                                    {{ __("Mot de passe") }}
                                </a>
                                <div class="dropdown-divider"></div>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <button class="dropdown-item btn btn-link" type="submit">
                                        {{ __("Se Déconnecter") }}
                                    </button>
                                </form>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>