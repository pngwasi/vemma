<nav id="sidebar" class="active">
    <div class="p-4">
        <h1>
            <a href="{{ route('dashboard.home') }}" class="logo">{{ $app_name }}</a>
        </h1>
        <ul class="list-unstyled components mb-5">
            <li class="{{ request()->url() == route('dashboard.home') ? 'active' : '' }}">
                <a href="{{ route('dashboard.home') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-home">
                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                    </svg>
                    <span>{{ __("Tableau de bord") }}</span>
                </a>
            </li>
            <li class="{{ Str::contains(request()->url(), route('dashboard.genealogy')) ? 'active' : '' }}">
                <a href="{{ route('dashboard.genealogy') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-git-branch">
                        <line x1="6" y1="3" x2="6" y2="15"></line>
                        <circle cx="18" cy="6" r="3"></circle>
                        <circle cx="6" cy="18" r="3"></circle>
                        <path d="M18 9a9 9 0 0 1-9 9"></path>
                    </svg>
                    <span>{{ __("Généalogie") }}</span>
                </a>
            </li>
            <li class="{{ Str::contains(request()->url(), route('dashboard.encashment')) ? 'active' : '' }}">
                <a href="{{ route('dashboard.encashment') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-dollar-sign">
                        <line x1="12" y1="1" x2="12" y2="23"></line>
                        <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
                    </svg>
                    {{ __("Encaissement") }}
                </a>
            </li>
        </ul>

        <div class="footer">
            <p> Copyright &copy;
                <script>
                    document.write(new Date().getFullYear());
                </script>
                {{ __("Tous les droits sont réservés") }}
                <span>| made with <b><a href="#" style="color: inherit" target="_blank">PNG_</a></b></span>
            </p>
        </div>

    </div>
</nav>