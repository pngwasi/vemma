const mix = require('laravel-mix');
const path = require('path')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('assets/app.js', '')
    .js("assets/modules/livewire-frame.js", "")
    .preact()
    .sass('assets/app.scss', '')
    .setPublicPath('public/assets/')
    .setResourceRoot('/assets/')
    .options({
        terser: {
            extractComments: false,
        }
    })
    .webpackConfig({
        resolve: {
            alias: {
                "@root": path.resolve(__dirname, 'assets/'),
                "react": "preact/compat",
                "react-dom/test-utils": "preact/test-utils",
                "react-dom": "preact/compat",
            },
        },
        output: {
            publicPath: '/assets/',
        }
    })
    .version()
    .extract(['stimulus'])
    .sourceMaps(false)
    .disableNotifications();
