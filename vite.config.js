import { defineConfig } from "vite";
import preact from "@preact/preset-vite";
import { resolve } from "path";
import StimulusHMR from "vite-plugin-stimulus-hmr";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [preact(), StimulusHMR()],
    base: "/dev/",
    root: "./assets",
    build: {
        rollupOptions: {
            input: ["app.js"],
        },
    },
    resolve: {
        alias: {
            "@root": resolve(__dirname, "assets/"),
            react: "preact/compat",
            "react-dom/test-utils": "preact/test-utils",
            "react-dom": "preact/compat",
        },
    },
});
