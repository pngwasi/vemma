<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRootSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->hasProfile(1, [
                'firstname' => 'Vemma',
                'lastname' => 'root',
            ])
            ->create([
                'username' => 'vemma_root',
                'email' => 'vemma.root@gmail.com',
                'root' => true
            ]);
    }
}
