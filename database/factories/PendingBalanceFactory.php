<?php

namespace Database\Factories;

use App\Models\PendingBalance;
use Illuminate\Database\Eloquent\Factories\Factory;

class PendingBalanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PendingBalance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
