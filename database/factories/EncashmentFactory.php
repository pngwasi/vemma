<?php

namespace Database\Factories;

use App\Models\Encashment;
use Illuminate\Database\Eloquent\Factories\Factory;

class EncashmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Encashment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
